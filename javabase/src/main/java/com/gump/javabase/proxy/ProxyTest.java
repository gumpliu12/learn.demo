package com.gump.javabase.proxy;

import java.lang.reflect.Proxy;

/**
 * @Description: TODO
 * @Author gumpLiu
 * @Date 2022-08-26
 * @Version V1.0
 **/
public class ProxyTest {
    public static void main(String[] args) {
        System.setProperty("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
        ApplicationService service = new ApplicationServiceImpl();
        ProxyInvocationHandler handler = new ProxyInvocationHandler(service);

        ApplicationService proxyBean = (ApplicationService)Proxy.newProxyInstance(ProxyTest.class.getClassLoader(), new Class<?>[]{ApplicationService.class}, handler);
    }
}
