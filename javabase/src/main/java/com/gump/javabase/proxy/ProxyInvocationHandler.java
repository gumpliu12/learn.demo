package com.gump.javabase.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @Description: 代理对象处理类
 * @Author gumpLiu
 * @Date 2022-08-26
 * @Version V1.0
 **/
public class ProxyInvocationHandler implements InvocationHandler {

    private Object source;

    public ProxyInvocationHandler(Object source){
        this.source = source;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return method.invoke(source, args);
    }
}
