package com.gump.javabase.proxy;

/**
 * @Description: proxy 测试接口
 * @Author gumpLiu
 * @Date 2022-08-26
 * @Version V1.0
 **/
public interface ApplicationService {

    public Object findById();
}
