package com.gump.javabase.proxy;

/**
 * @Description: 代理测试实现类
 * @Author gumpLiu
 * @Date 2022-08-26
 * @Version V1.0
 **/
public class ApplicationServiceImpl implements ApplicationService {
    @Override
    public Object findById() {
        return new Object();
    }
}
