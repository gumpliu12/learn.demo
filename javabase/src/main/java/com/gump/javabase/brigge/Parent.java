package com.gump.javabase.brigge;

// 定义一个范型接口
public interface Parent<T> {
    T bridgeMethod(T param);
}