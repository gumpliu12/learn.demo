package com.gump.javabase.brigge;

import java.lang.reflect.Method;

public class BridgeMethodTest {
    public static void main(String[] args) throws Exception {
       // 使用java的多态
        Parent parent = new Child();
        System.out.println(parent.bridgeMethod("abc123"));// 调用的是实际的方法
        Class<? extends Parent> clz = parent.getClass();
        Method method = clz.getMethod("bridgeMethod", Object.class); // 获取桥接方法
        System.out.println(method.isBridge()); // true
        System.out.println(method.invoke(parent, "hello")); // 调用的是桥接方法
        System.out.println(parent.bridgeMethod(new Object()));// 调用的是桥接方法, 会报ClassCastException: java.lang.Object cannot be cast to java.lang.String`错误`
    }
}