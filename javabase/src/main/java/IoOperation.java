import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class IoOperation {
    static byte[] data = "1234567890\n".getBytes();
    static String path = "/home/id-server/idTest.txt";
    static int count = 0;
    public static void main(String[] args) throws Exception {
//        switch (args[0]) {
//            case "0":
//                testBasicFileIO();
//                break;
//            case "1":
//                testBufferedFileIO();
//                break;
//            default:
//
//        }
        Thread thread1 = new Thread(new MyThread());
        Thread thread2 = new Thread(new MyThread());
        Thread thread3 = new Thread(new MyThread());
        Thread thread4 = new Thread(new MyThread());
        Thread thread5 = new Thread(new MyThread());
        Thread thread6 = new Thread(new MyThread());
        Thread thread7 = new Thread(new MyThread());
        Thread thread8 = new Thread(new MyThread());
        Thread thread9 = new Thread(new MyThread());
        thread1.setName("thread1");
        thread2.setName("thread2");
        thread3.setName("thread3");
        thread4.setName("thread4");
        thread5.setName("thread5");
        thread6.setName("thread6");
        thread7.setName("thread7");
        thread8.setName("thread8");
        thread9.setName("thread9");
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
        thread6.start();
        thread7.start();
        thread8.start();
        thread9.start();
        Thread.currentThread().wait(1000000000);
    }
    public static class MyThread implements Runnable{

        @Override
        public void run() {
            while (true){
                try {
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void testBasicFileIO() throws Exception {
        File file = new File(path);
        if(file.exists()){
            file.createNewFile();
        }
        FileOutputStream out = new FileOutputStream(file);
        final long start = System.currentTimeMillis();
        while (count < 10000) {
            out.write(data);
            count++;
        }
        System.out.println(System.currentTimeMillis() - start);
        out.close();
    }
    public static void testBufferedFileIO() throws Exception {
        File file = new File(path);
        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
        final long start = System.currentTimeMillis();
        while (count < 10000) {
            out.write(data);
            count++;
        }
        System.out.println(System.currentTimeMillis() - start);
        out.close();
    }
}
