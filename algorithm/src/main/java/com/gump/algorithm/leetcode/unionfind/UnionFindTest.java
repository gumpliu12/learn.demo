package com.gump.algorithm.leetcode.unionfind;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 并查集练习
 * @Author gumpLiu
 * @Date 2022-08-25
 * @Version V1.0
 **/
public class UnionFindTest {

    public static void main(String[] args) {
        int[] nums = {1,3,2,4,5};
        UnionFind unionFind = new UnionFind(nums);

        for(int i : nums){
            if(unionFind.parentMap.containsKey(i+1)){
                unionFind.union(i, i+1);
            }
        }
        for(int i : nums){
            unionFind.find(i);
        }
        int count = 0;
        for(Map.Entry<Integer, Integer> entry : unionFind.parentMap.entrySet()){
            count = Math.max(count, entry.getValue() - entry.getKey() + 1);
        }

        System.out.println(count);

    }

    static class UnionFind{
        public Map<Integer, Integer> parentMap = new HashMap<Integer, Integer>();

        public UnionFind(int[] nums){
            for(int i : nums){
                parentMap.put(i, i);
            }
        }

        public Integer find(int x){
            if(!parentMap.containsKey(x)) return null;
            int xx = x;
            while (xx != parentMap.get(xx)){
                if(parentMap.containsKey(parentMap.get(xx))){
                    parentMap.put(x, parentMap.get(parentMap.get(xx)));
                }else{
                    parentMap.put(x, parentMap.get(xx));
                }
                xx = parentMap.get(xx);
            }
            return xx;
        }

        public void union(int x, int y){
            Integer xvalue = find(x);
            Integer yvalue = find(y);
            if(xvalue != yvalue){
                parentMap.put(xvalue, yvalue);
            }
        }

    }
}
