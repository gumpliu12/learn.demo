package com.gump.algorithm.leetcode;

import java.util.HashMap;
import java.util.Map;

public class LRUCache {
    public Node head, tail;
    public int capacity, size;
    public Map<Integer, Node> cacheMap = new HashMap<Integer, Node>();

    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.size = 0;
        this.head = new Node();
        this.tail = new Node();
        head.next = tail;
        tail.pre = head;
    }
    
    public int get(int key) {
        Node node = cacheMap.get(key);
        if(node != null){
            remove(node);
            addHead(node);
            return node.value;
        }
        return -1;
    }
    
    public void put(int key, int value) {
        Node node = cacheMap.get(key);
        if(node == null){
            node = new Node(value, key);
            addHead(node);
            cacheMap.put(key, node);
            ++size;
            if(size > capacity){
                cacheMap.remove(removeTail().key);
                --size;
            }
        }else{
            node.value = value;
            remove(node);
            addHead(node);
        }
    }

    private void addHead(Node node){
        Node curHead = head.next;
        head.next = node;
        node.next = curHead;
        node.pre = head;
        curHead.pre = node;
    }

    private void remove(Node node){
        node.pre.next = node.next;
        node.next.pre = node.pre;
    }

    private Node removeTail(){
        Node curPre = tail.pre;
        remove(curPre);
        return curPre;
    }


    class Node{
        public int value;
        public int key;
        public Node next;
        public Node pre;
        public Node(){}
        public Node(int value, int key){
            this.value = value;
            this.key = key;
        }
    }
    public static void main(String[] args) {
        LRUCache lruCache = new LRUCache(2);
        lruCache.put(1,1);
        lruCache.put(2,2);
        lruCache.get(1);
        lruCache.put(3,3);
        System.out.println(lruCache.get(2));
    }
}