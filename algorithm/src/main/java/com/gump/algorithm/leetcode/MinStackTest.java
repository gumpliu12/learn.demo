package com.gump.algorithm.leetcode;

import java.util.Stack;

/**
 * @Description: 最小栈
 * 设计一个支持 push ，pop ，top 操作，并能在常数时间内检索到最小元素的栈。
 * 实现 MinStack 类:
 *      MinStack() 初始化堆栈对象。
 *      void push(int val) 将元素val推入堆栈。
 *      void pop() 删除堆栈顶部的元素。
 *      int top() 获取堆栈顶部的元素。
 *      int getMin() 获取堆栈中的最小元素。
 * @Author gumpLiu
 * @Date 2022-08-25
 * @Version V1.0
 **/
public class MinStackTest {
    public static void main(String[] args) {
        MinStack minStack = new MinStack();
        /**
         ["MinStack","push","push","push","getMin","pop","top","getMin"]
         [[],[-2],[0],[-3],[],[],[],[]]
         */
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        minStack.getMin();
        minStack.pop();
        minStack.top();
        minStack.getMin();
    }

}
class MinStack {

    public Stack<Integer> queue = new Stack();
    public Stack<Integer> minqueue = new Stack();
    public int minvalue = Integer.MAX_VALUE;
    public int size;
    public MinStack() {
    }

    public void push(int val) {
        queue.push(val);
        minvalue = Math.min(val, minvalue);
        minqueue.push(minvalue);
        ++size;
    }

    public void pop() {
        queue.pop();
        minqueue.pop();
        if(!minqueue.isEmpty()){
            minvalue = minqueue.peek();
        }else{
            minvalue = Integer.MAX_VALUE;
        }
        --size;
    }

    public int top() {
        return queue.peek();
    }

    public int getMin() {
        return minvalue;
    }
}