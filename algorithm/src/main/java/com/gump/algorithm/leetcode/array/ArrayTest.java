package com.gump.algorithm.leetcode.array;

import java.util.regex.Pattern;

/**
 * @Description: 数字类型
 * @Author gumpLiu
 * @Date 2022-09-01
 * @Version V1.0
 **/
public class ArrayTest {
    public static void main(String[] args) {

        int[] height = {0,1,0,2,1,0,1,3,2,1,2,1};

        char[][] chars =
                {{'1','1','1','1','1','1','1','1'},
                 {'1','1','1','1','1','1','1','0'},
                 {'1','1','1','1','1','1','1','0'},
                 {'1','1','1','1','1','0','0','0'},
                 {'0','1','1','1','1','0','0','0'}};
        char[][] chars1 = {{'1','0','1','0','0'},{'1','0','1','1','1'},{'1','1','1','1','1'},{'1','0','0','1','0'}};
        System.out.println(maximalRectangle(chars));
    }



    /**
     * 给定 n 个非负整数表示每个宽度为 1 的柱子的高度图，计算按此排列的柱子，下雨之后能接多少雨水
     * @param height
     * @return
     */
    public static int trap(int[] height) {
        if(height == null || height.length <= 2) return 0;
        //从左—>右
        int left = 0;
        int count = 0;
        for(int right = 1; right < height.length; right++){
            if(height[right] >= height[left]){
                if(right - left == 1) {
                    left = right;
                    continue;
                }else {
                    for(int i = left + 1; i < right; i++){
                        count+= height[left] - height[i];
                    }
                    left = right;
                }
            }
        }
        int right = height.length - 1;
        for(int l = height.length - 2; l >= 0; l--){
            if(height[l] > height[right]){
                if(right - l == 1){
                    right = l;
                    continue;
                }else{
                    for(int i = l + 1; i < right; i++){
                        count += height[right] - height[i];
                    }
                    right = l;
                }
            }
        }

        return count;
    }

    /**
     * 给定一个仅包含 0 和 1 、大小为 rows x cols 的二维二进制矩阵，
     * 找出只包含 1 的最大矩形，并返回其面积。
     *
     * @param matrix
     * @return
     */
    public static int maximalRectangle(char[][] matrix) {
        if(matrix == null || matrix.length == 0) return 0;

        int lenstart = 0, lenend = 0, widestart = 0, wideend = 0, maxCount = 0;
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[0].length; j++){
                if(matrix[i][j] == '1'){
                    lenstart = j== -1 ? j : j-1; widestart = i == -1 ? i : i-1; lenend = j; wideend = i;
                    int len = matrix[0].length - 1;
                    for(int ii = i; ii < matrix.length; ii++){
                        if(matrix[ii][j] == '0') break;
                        for(int jj = j; jj <=  len; jj++){
                            if(matrix[ii][jj] == '1'){
                                lenend = Math.max(lenend, jj);
                            }else{
                                len = jj - 1;
                                break;
                            }
                        }
                    }

                    if (lenend - lenstart < 0) continue;

                    int wide = matrix.length-1;
                    for(int ii = i; ii <= wide; ii++){
                        if(matrix[ii][j] == '0') break;
                        for(int jj = j; jj <=  lenend; jj++){
                            if(matrix[ii][jj] == '1' && jj == lenend){
                                wideend = Math.max(wideend, ii);
                                maxCount = Math.max((lenend-lenstart) * (wideend-widestart), maxCount);
                            }
                            if(matrix[ii][jj] == '0'){
                                lenend = jj - 1;
                                wideend = ii;
                                maxCount = Math.max((lenend-lenstart) * (wideend-widestart), maxCount);
                                break;
                            }
                        }
                    }
                }
            }
        }

        return maxCount;
    }
}
