package com.gump.algorithm;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
	//直接求解可以通过60%
	public static void main(String[] args)
	{
		List<String> list = new ArrayList<>();
		for(int i = 0; i< 100; i++) {
			Date date = DateUtil.stringtoDate(DateUtil.getCurrent(), "yyyy-MM-dd");
			list.add(date + "-cache-DEFAULT-FORMAT_PREFIXYYYYMMDD00000000000");
		}
		for(int i = 1; i < list.size(); i++){
			System.out.println(list.get(i-1).equals(list.get(i)));
		}
	}

	public static int findLength(int[] nums1, int[] nums2) {
		return nums1.length<=nums2.length? findMax(nums1,nums2):findMax(nums2,nums1);
	}

	public static int findMax(int[] nums1, int[] nums2){
		int max=0;
		int m=nums1.length,n=nums2.length;
		/**
		 nums1,nums2中较短的数组不动，这里默认nums1，较长的数组滑动
		 初始位置：nums2右边界挨着nums1左边界，nums2从左往右滑动
		 */
		// 第一阶段：nums2从左往右滑动，两数组重合部分长度不断增加，重合部分长度len从1开始增加
		// 重合部分：nums1起点下标0，nums2起点下标n-len，
		/**
			                          1,2,3,4,5,6,7,8,9
						    1,2,3,4,5,6
		 *
		 */
		for(int len=1;len<=m;len++){
			max=Math.max(max,maxLen(nums1,0,nums2,n-len,len));
		}
		// 第二阶段：nums2从左往右滑动，两数组重合部分长度不变，重合部分长度始终为nums1长度m
		//  重合部分：nums1起点下标0，nums2起点下标n-m，然后递减
		for(int j=n-m;j>=0;j--){
			max=Math.max(max,maxLen(nums1,0,nums2,j,m));
		}
		// 第三阶段：nums2从左往右滑动，两数组重合部分长度递减，重合部分长度始终为nums1长度m-i
		//  重合部分：nums1起点下标i，递增，nums2起点下标0
		for(int i=1;i<m;i++){
			max=Math.max(max,maxLen(nums1,i,nums2,0,m-i));
		}
		return max;
	}

	/**
	 nums1中下标i开始，nums2中下标j开始，长度为len子数组中，最长公共子数组(注意要连续)长度
	 */
	public static int maxLen(int[] nums1,int i,int[] nums2,int j,int len) {
		int count = 0, res = 0;
		for (int k = 0; k < len; k++) {
			if (nums1[i + k] == nums2[j + k]) {
				count++;
			} else if (count > 0) {
				//进入到这个if判断体里面，说明当前 nums1[i+k]!=nums2[j+k],即之前的公共子数组不再连续，
				// 所以要记录最大值，同时将count置零
				res = Math.max(count, res);
				count = 0;
			}
		}
		/**
		 1，count>0,说明有公共子数组是以nums1[i+len-1],nums2[j+len-1]结尾的，
		 上面最后一步for循环没有进入到else if判断题里面，所以最终结果要取当前count和res的最大值
		 2，count=0，说明res已经更新过了，res即为最终结果
		 */
		return count > 0 ? Math.max(count, res) : res;
	}



	public int[][] generateMatrix(int n) {
		int maxNum = n * n;
		int curNum = 1;
		int[][] matrix = new int[n][n];
		int row = 0, column = 0;
		int[][] directions = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}}; // 右下左上
		int directionIndex = 0;
		while (curNum <= maxNum) {
			matrix[row][column] = curNum;
			curNum++;
			int nextRow = row + directions[directionIndex][0], nextColumn = column + directions[directionIndex][1];
			if (nextRow < 0 || nextRow >= n || nextColumn < 0 || nextColumn >= n || matrix[nextRow][nextColumn] != 0) {
				directionIndex = (directionIndex + 1) % 4; // 顺时针旋转至下一个方向
			}
			row = row + directions[directionIndex][0];
			column = column + directions[directionIndex][1];
		}
		return matrix;
	}


	public static List<Integer> spiralOrder(int[][] matrix) {

		List<Integer> values = new ArrayList<Integer>();
		int sum = matrix.length * matrix[0].length;
		int up = 0, down = matrix.length - 1, left = 0, right = matrix[0].length - 1, value = 1;
		while( value <= sum){
			for(int i = up; i <= right; i++){
				if(value > sum) return values;
				values.add(matrix[up][i]);
				value++;
			}
			up++;
			for(int i = up; i <= down; i++){
				if(value > sum) return values;
				values.add(matrix[i][right]);
				value++;
			}
			right--;

			for(int i = right ; i >= left; i--){
				if(value > sum) return values;
				values.add(matrix[down][i]);
				value++;
			}
			down--;

			for(int i = down; i >= up; i--){
				if(value > sum) return values;
				values.add(matrix[i][left]);
				value++;
			}
			left++;
		}
		return values;
	}
	public static int totalFruit(int[] fruits) {
		if(fruits.length <= 2) return fruits.length;

		int var1 = fruits[0], var2 = fruits[1];
		int left = 0, result = 2;
		for(int rigth = 2; rigth < fruits.length; rigth++){
			if (var1 == var2){
				var2 = fruits[rigth];
				continue;
			}
			int cur = fruits[rigth];
			if(var1 != cur  && var2 != cur){
				var1 = fruits[rigth -1];
				var2 = fruits[rigth];
				result = Math.max(rigth - left, result);
				int curLeft = rigth - 1;
				while (fruits[curLeft - 1] == var1){
					curLeft--;
				}
				left = curLeft;

			}
		}
		return Math.max(fruits.length - left, result);
	}
}