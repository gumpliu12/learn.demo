package com.gump.algorithm.number;

import java.math.BigInteger;

/**
 * 数字计算
 *
 * @author gumpliu
 * @create 2021-05-18 21:31
 */
public class IntegerNumber {


  public static void main(String[] args){

    sqrt1(6);

  }

  /**
   * 求平方根
   * x = e^ 1/2 * lneX
   * @return
   */
  public static int sqrt(int num){
    if(num == 0) return 0;

    int anx = Double.valueOf(Math.exp(0.5 * Math.log(num))).intValue();

    long sum = (long)((anx+1)*(anx+1));

    return sum <= num ? anx + 1 : anx;

  }

  /**
   * 求平方根
   * x = e^ 1/2 * lneX
   * @return
   */
  public static int sqrt1(int num){
    if(num == 0) return 0;

    int left = 0, right = num, ans = -1;

    while (left <= right){

      int mid = left + ((right - left)/2);
      BigInteger sum = BigInteger.valueOf(mid).multiply(BigInteger.valueOf(mid));

      if(sum.compareTo(BigInteger.valueOf(num)) < 1){
        ans = mid;
        left = mid + 1;
      }else{
        right = mid - 1;
      }

    }

    return ans;

  }


}
