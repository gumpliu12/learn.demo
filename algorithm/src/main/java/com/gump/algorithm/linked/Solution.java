package com.gump.algorithm.linked;

class Solution {
    public ListNode swapPairs(ListNode head) {
        if(head == null || head.next == null){
            return head;
        }
        ListNode next = head.next;

        ListNode node = swapPairs(next.next);

        next.next = head;
        head.next = node;
        return next;
    }

    public ListNode swapPairs1(ListNode head) {
        if(head == null || head.next == null){
            return head;
        }
        ListNode dmp = new ListNode();
        dmp.next = head;
        ListNode curr = dmp;
        while (curr.next != null && curr.next.next != null){
            ListNode tmp = curr.next;
            ListNode tmp1 = curr.next.next.next;

            curr.next = curr.next.next;
            curr.next.next = tmp;
            curr.next.next.next = tmp1;

            curr = curr.next.next;
        }

        return dmp.next;
    }

    public ListNode reverseList(ListNode head) {
        ListNode pre = null;
        ListNode curr = head;

        while (curr != null && curr.next != null){
            ListNode tmp = curr.next;
            curr.next = pre;
            pre = curr;
            curr = tmp;
        }

        return pre;
    }

    public ListNode reverseList1(ListNode head) {
        if(head == null || head.next == null){
            return head;
        }
        ListNode curr = head;
        ListNode node = reverseList1(curr.next);

        curr.next.next = curr;
        curr.next = null;

        return node;
    }


}