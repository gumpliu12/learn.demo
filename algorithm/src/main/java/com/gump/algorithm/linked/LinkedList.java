package com.gump.algorithm.linked;

import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 链表操作
 *
 * @author gumpliu
 * @create 2021-04-22 22:15
 */
public class LinkedList {

  public static void main(String[] args) {
    ListNode node1 = new ListNode(1);
    ListNode node2 = new ListNode(2);
    ListNode node3 = new ListNode(3);
    ListNode node4 = new ListNode(4);
    ListNode node5 = new ListNode(5);
    ListNode node6 = new ListNode(6);
    ListNode node7 = new ListNode(7);
    ListNode node8 = new ListNode(8);
    ListNode node9 = new ListNode(9);
    ListNode node10 = new ListNode(10);
    ListNode node11 = new ListNode(11);
    ListNode node12 = new ListNode(12);
    node1.next = node2;
    node2.next = node3;
    node3.next = node4;
    node4.next = node5;
    node5.next = node6;
    node6.next = node7;
//    node7.next = node8;
//    node8.next = node9;
//    node9.next = node10;
//    node10.next = node11;
//    node11.next = node12;
//    splitListToParts(node1, 3);
//    int[] nums = {5,4,1,1,5,1,5};
//    foundOnceNumber(nums, 3);

    System.out.println(6 & 15);
    System.out.println(8 ^ 6 ^ 9 ^ 7 ^ 6 ^ 7 ^ 9);
    swapPairs2(node1);

  }

  //反正链表
  // 1 -> 2 -> 3 -> 4 -> 5 (5 -> 4 -> 3 -> 2 -> 1)
  public ListNode reverseList(ListNode head) {
      if(head == null || head.next == null) return head;
      ListNode node = reverseList(head.next);
      head.next.next = head;
      head.next = null;
      return node;
  }
  public ListNode reverseList1(ListNode head) {
    if(head == null || head.next == null) return head;
    ListNode pre = null, cur = head;
    while (cur != null){
      ListNode tmp = cur.next;
      cur.next = pre;
      pre = cur;
      cur = tmp;
    }
    return pre;
  }

  /**
   * 旋转链表，两个节点
   * @param head
   * @return
   */
  // 1 -> 2 -> 3 -> 4 转换成 2 -> 1 -> 4 -> 3
  public static ListNode swapPairs(ListNode head) {
    if(head == null || head.next == null) return head;

    ListNode cur = head, pre = null, new_head = null;
    while (cur != null && cur.next != null){
      ListNode next_cur = cur.next.next;
      ListNode tmp = cur.next;
      tmp.next = cur;
      tmp.next.next = next_cur;
      if(pre == null){
        new_head = tmp;
        pre = cur;
      }else {
        pre.next = tmp;
        pre = cur;
      }
      cur = next_cur;
    }
    return new_head;
  }
  // 1 -> 2 -> 3 -> 4 转换成 2 -> 1 -> 4 -> 3
  public static ListNode swapPairs1(ListNode head) {
    if(head == null || head.next == null) return head;
    ListNode virtualNode = new ListNode();
    virtualNode.next = head;
    ListNode cur = virtualNode;
    while (cur != null && cur.next != null && cur.next.next != null){
      ListNode tmp = cur.next;//1
      ListNode tmp1 = cur.next.next.next;//3
      cur.next = cur.next.next;
      cur.next.next = tmp;
      cur.next.next.next = tmp1;
      cur = cur.next.next;//返回翻转最后一个节点，相当于虚拟节点
    }
    return virtualNode.next;
  }
  // 1 -> 2 -> 3 -> 4 转换成 2 -> 1 -> 4 -> 3
  public static ListNode swapPairs2(ListNode head) {
    if(head == null || head.next == null) return head;
    ListNode node = swapPairs2(head.next.next);
    ListNode tmp = head.next;
    tmp.next = head;
    tmp.next.next = node;
    return tmp;
  }


  public ListNode reverseKGroup1(ListNode head, int k) {
    ListNode hair = new ListNode(0);
    hair.next = head;
    ListNode pre = hair;
    while (head != null) {
      ListNode tail = pre;
      // 查看剩余部分长度是否大于等于 k
      for (int i = 0; i < k; ++i) {
        tail = tail.next;
        if (tail == null) {
          return hair.next;
        }
      }
      ListNode nex = tail.next;
      ListNode[] reverse = myReverse(head, tail);
      head = reverse[0];
      tail = reverse[1];
      // 把子链表重新接回原链表
      pre.next = head;
      tail.next = nex;
      pre = tail;
      head = tail.next;
    }

    return hair.next;
  }

  public ListNode[] myReverse(ListNode head, ListNode tail) {
    ListNode prev = tail.next;
    ListNode p = head;
    while (prev != tail) {
      ListNode nex = p.next;
      p.next = prev;
      prev = p;
      p = nex;
    }
    return new ListNode[]{tail, head};
  }



  public static ListNode detectCycle(ListNode head) {
    if(head == null || head.next == null) return null;

    ListNode fast = head, show = head;

    while(fast != null && fast.next != null && show != null){
      show = show.next;
      fast = fast.next.next;
      if(show == fast){
        show = head;
        while(show != fast){
          show = show.next;
          fast = fast.next;
          if(show == fast) return fast;
        }
      }
    }
    return null;
  }

  private static ListNode reverseNode(ListNode head) {
    if(head == null || head.next == null){
      return head;
    }
    ListNode next = head.next;
    ListNode res = reverseNode(head.next);
    next.next = head;
    head.next = null;
    return res;
  }

  public static ListNode[] splitListToParts(ListNode head, int k) {
    ListNode[] list = new ListNode[k];
    if(head == null) return list;

    int len = 0;
    ListNode cur = head;
    while(cur != null){
      len++;
      cur = cur.next;
    }
    int i = 0;
    if(len <= k){
      while(head != null){
        ListNode temp = head.next;
        head.next = null;
        list[i] = head;
        i++;
        head = temp;
      }
      return list;
    }

    int max = len % k;
    while(head != null){
      list[i] = head;
      i++;
      ListNode pre = head;
      int num = len/k;
      boolean frist = true;
      while(head != null && num > 0){
        if(frist && max > 0){
          max--;
        }else{
          num--;
        }
        frist = false;
        pre = head;
        head = head.next;
      }
      pre.next = null;
    }
    return list;
  }
  public static int foundOnceNumber (int[] arr, int k) {

    Map<Integer, Integer> map = new HashMap<Integer, Integer>();
    for(int num : arr){

      if(map.containsKey(num)){
        int i = map.get(num) + 1;
        if(i == k) map.remove(num);
        else
          map.put(num, i);
      }else{
        map.put(num, 1);
      }
    }

    for (Map.Entry<Integer, Integer> entry : map.entrySet()){
      return entry.getKey();
    }

    return 0;

  }

  /**
   * 给定一个无序单链表，实现单链表的排序(按升序排序)。
   * @param head ListNode类 the head node
   * @return ListNode类
   */
  public static ListNode sortInList (ListNode head) {
    // write code here

    if(head == null || head.next == null) return head;
    ListNode tail = head.next;
    ListNode newHead = head;
    newHead.next = null;
    while (tail != null){
      newHead = getNextNode(tail.val, newHead);
      tail = tail.next;
    }

    return newHead;
  }

  public static ListNode getNextNode(int val, ListNode head){

    ListNode tail = head;
    ListNode perrNode = null;
    ListNode node = new ListNode(val);

    while (tail != null){
      if(tail.val > node.val){
        if(perrNode == null){
          node.next = head;
          return node;
        }else {
          perrNode.next = node;
          node.next = tail;
          return head;
        }

      }
      perrNode = tail;
      tail = tail.next;
    }
    perrNode.next = node;
    return head;
  }


  public List<Integer> printListFromTailToHead(ListNode listNode) {

    if(listNode == null) return Collections.emptyList();

    List<Integer> list = new ArrayList<Integer>();
    while (listNode != null){
      list.add(0, listNode.val);
      listNode = listNode.next;
    }

    return list;

  }

  /**
   * 输入一个链表，反转链表后，输出新链表的表头。
   * @param head
   * @return
   */
  public ListNode ReverseList(ListNode head){
    ListNode pre = null;
    ListNode cur = head;
    ListNode next = null;

    while (cur != null){
      //下一个节点
      next = cur.next;
      //转指针
      cur.next = pre;
      pre = cur;
      cur = next;
    }
    return pre;
  }

  public ListNode mergeKLists(ArrayList<ListNode> lists) {
    if(lists == null || lists.size() == 0) return null;

    return merge(lists, 0, lists.size() - 1);

  }

  public ListNode merge(ArrayList<ListNode> lists, int start, int end){
    if(start == end) return lists.get(start);

    if(start > end) return null;

    int mod = start + (end - start)/2;

    return mergeKLists(merge(lists, start, mod), merge(lists, mod + 1, end));
  }

  public ListNode mergeKLists(ListNode node1, ListNode node2) {

    if(node1 == null) return node2;
    if (node2 == null) return node1;

     ListNode heard = new ListNode();

    ListNode tail = heard;

    while (node1 != null && node2 != null){
      if(node1.val >= node2.val){
        tail.next = node2;
       node2 = node2.next;
      }else{
        tail.next = node1;
        node1 = node1.next;
      }
      tail = tail.next;
    }

    tail.next = node1 != null ? node1 : node2;

    return heard.next;
  }


  /**
   * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
   *
   * 输入一个链表，输出该链表中倒数第k个结点。
   * 如果该链表长度小于k，请返回空。
   *  输入： {1,2,3,4,5},1  输出： 5
   *
   * @param pHead ListNode类
   * @param k int整型
   * @return ListNode类
   */
  public ListNode FindKthToTail (ListNode pHead, int k) {

    if(pHead == null || k<=0) return null;

    ListNode tail = pHead;
    ListNode pare = null;

    while (tail != null){
      ListNode next = tail.next;
      tail.next = pare;
      pare = tail;
      tail = next;
    }

    ListNode tailNode = pare;
    ListNode pareNode = null;
    while (tailNode!= null) {
      ListNode next = tailNode.next;
      tailNode.next = pareNode;
      pareNode = tailNode;
      tailNode = next;

      if(--k <= 0){
        return pareNode;
      }
    }
    return null;
  }

  /**
   * 将给出的链表中的节点 k 个一组翻转，返回翻转后的链表
   * 如果链表中的节点数不是 k 的倍数，将最后剩下的节点保持原样
   * 你不能更改节点中的值，只能更改节点本身。
   * 要求空间复杂度  O(1)
   * 例如：
   * 给定的链表是1→2→3→4→5
   * 对于 k = 2 , 你应该返回2→1→4→3→5
   * 对于 k = 3 , 你应该返回 3→2→1→4→5
   * @param head ListNode类
   * @param k int整型
   * @return ListNode类
   */
  public static ListNode reverseKGroup (ListNode head, int k) {

    if(head == null || head.next == null || k == 1) return head;

    ListNode pre_tail = null;//上尾节点

    ListNode next_head = head;

    ListNode firstHead = null;

    while (next_head != null){
      ListNode[] nodes =  reverseNodeK(next_head, k);
      next_head = nodes[2];//下一头节点

      if(firstHead == null){
        firstHead = nodes[1];
        pre_tail = nodes[0];
      }else{
        pre_tail.next = nodes[1];
        pre_tail = nodes[0];
      }
    }

    return firstHead;
  }

  public static ListNode[] reverseNodeK(ListNode head, int k){
    ListNode[] nodes = new ListNode[3];

    ListNode node = head;
    int length = 0;

    while (node != null) {
      ++length;
      node = node.next;
    }

    if(length < k){
      nodes[1] = head;

      return nodes;
    }


    ListNode pre = null;//前节点
    ListNode tail = head;//转化后尾指针
    while (tail != null && k > 0){
      ListNode next = tail.next;
      tail.next = pre;
      pre = tail;
      tail = next;
      --k;
    }
    nodes[0] = head;
    nodes[1] = pre;

    if(k > 0){
      return nodes;
    }
    nodes[2] = tail;

    return nodes;
  }


  /**
   * 给定一个链表，删除链表的倒数第 n 个节点并返回链表的头指针
   * 例如，
   * 给出的链表为: 1→2→3→4→5, n=2.
   * 删除了链表的倒数第 n 个节点之后,链表变为1→2→3→5.
   *
   * 备注：
   * 题目保证 n 一定是有效的
   * 请给出请给出时间复杂度为O(n)的算法
   *
   * @param head ListNode类
   * @param n int整型
   * @return ListNode类
   */
  public static ListNode removeNthFromEnd (ListNode head, int n) {
    // write code here
    ListNode tail = head;
    ListNode deletePreNode = null;
    int k = 0;
    while (tail != null){
      ++k;

      if(k >= n + 1){
        if(k == n + 1){
          deletePreNode = head;
        }else {
          deletePreNode = deletePreNode.next;
        }
      }
      tail = tail.next;
    }

    if(deletePreNode == null) {
      head = head.next;
    }else{
      ListNode deleteNode = deletePreNode.next;
      deletePreNode.next = deleteNode.next;
      deleteNode.next = null;
    }

    return head;
  }



}


class ListNode{
  public   int val;
  public ListNode next;

  public ListNode(){
  }


  public ListNode(int val){
    this.val = val;
  }
}