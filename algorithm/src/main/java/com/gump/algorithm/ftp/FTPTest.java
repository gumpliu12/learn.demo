package com.gump.algorithm.ftp;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPFile;
import it.sauronsoftware.ftp4j.connectors.SOCKS5Connector;

//import org.apache.commons.net.ftp.FTPFile;

//import java.net.InetAddress;
//import java.net.InetSocketAddress;
//import java.net.Proxy;
//import java.net.SocketAddress;

public class FTPTest {
    public static void main(String[] args) {

        try {
            FTPClient ftpClient = new FTPClient();
            SOCKS5Connector socks5Connector = new SOCKS5Connector("192.168.99.219", 1081);
            socks5Connector.setReadTimeout(60);
            socks5Connector.setConnectionTimeout(60);
            ftpClient.setConnector(socks5Connector);
            ftpClient.connect("ftp.csindex.com.cn", 20021);
            ftpClient.login("csictcn","86397818");
            FTPFile[] files = ftpClient.list();
            for(FTPFile file : files){
                System.out.println(file.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        System.setProperty("jdk.net.hosts.file", "/foo");
//        SocketAddress proxyAddress = new InetSocketAddress("192.168.99.219", 1081);
//        Proxy proxy = new Proxy(Proxy.Type.SOCKS, proxyAddress);
//        SofaFTPClient client = new SofaFTPClient();
//        client.setProxy(proxy);
//        client.setRemoteVerificationEnabled(false);
//
//        try {
//            InetAddress inetAddress = InetAddress.getByName("ftp.csindex.com.cn");
//            client.connect(inetAddress, 20021);
//            client.setBufferSize(1024*1024*1024);
//            client.setDataTimeout(1000 * 60 * 60);
//            client.setDefaultTimeout(1000 * 60 * 60);
//            client.setRemoteVerificationEnabled(false);
//            client.setControlEncoding("UTF-8");
//            Boolean isLogin = client.login("csictcn","86397818");
//            client.enterLocalPassiveMode();
//            FTPFile[] files = client.listDirectories();
//            for(FTPFile ftpFile : files){
//                System.out.println(ftpFile.getName());
//            }
//
//        } catch (Throwable e) {
//            e.printStackTrace();
//        }
    }
}
