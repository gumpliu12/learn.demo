package com.gump.algorithm.ftp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketAddress;

class MySocket extends Socket {

	public MySocket(Proxy connProxy) {
		super(connProxy);
	}

	public MySocket() {
		super();
	}

	public void connect(SocketAddress endpoint, int timeout) throws IOException {
		InetSocketAddress endpoint2 = (InetSocketAddress) endpoint;
		if (endpoint2.getHostName().equals("ftp.csindex.com.cn")) {
			endpoint2 = InetSocketAddress.createUnresolved("ftp.csindex.com.cn", 20021);
		}
		endpoint2 = InetSocketAddress.createUnresolved("ftp.csindex.com.cn", 20021);
		super.connect(endpoint2, timeout);
	}
}