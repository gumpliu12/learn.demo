//package com.gump.algorithm.ftp;
//
//import com.sun.corba.se.impl.legacy.connection.DefaultSocketFactory;
//
//import javax.net.SocketFactory;
//import java.io.IOException;
//import java.lang.reflect.Field;
//import java.lang.reflect.Type;
//import java.net.InetSocketAddress;
//import java.net.Proxy;
//import java.net.Socket;
//import java.net.SocketAddress;
//
//class MySocketFactory extends DefaultSocketFactory {
//		private final Proxy connProxy;
//
//		public MySocketFactory(Proxy proxy) {
//			super(proxy);
//			connProxy = proxy;
//		}
//
//		public Socket createSocket() throws IOException {
//			if (this.connProxy != null) {
//				return new MySocket(connProxy);
//			}
//			return new MySocket();
//		}
//
//		public boolean agentTest(String protocol, String agentId, String host, String port,
//								 String user, String password) throws Exception {
//			boolean flag = false;
//			ImportAccessAgent agent = (ImportAccessAgent) importAccessAgentBO.findById(agentId);
//
//			if ("ftp".equals(protocol)) {
//				Type type = Type.HTTP;
//				SocketAddress proxyAddress = new InetSocketAddress(agent.getHost(), agent.getPort());
//				if (agent.getProtocol().toLowerCase().contains("sock")) {
//					type = Type.SOCKS;
//				}
//				Proxy proxy = new Proxy(type, proxyAddress);
//				FTPClient client = new FTPClient();
//				SocketFactory factory = new MySocketFactory(proxy);
//				client.setSocketFactory(factory);
//
//				Class<SocketClient> cl = SocketClient.class;
//				Field proxyFid = cl.getDeclaredField("connProxy");
//				proxyFid.setAccessible(true);
//				proxyFid.set(client, proxy);
//
//				logger.info("agentHost:" + agent.getHost() + " agentPort:" + agent.getPort() + " host:" + host + " port:" + port);
//
//				InetAddress address = InetAddress.getByAddress(host, new byte[] { 0,0,0,0});
//			/*client.setProxy(proxy);
//			InetAddress address = InetAddress.getByName(host);*/
//				if (StringUtil.isNotEmpty(port)) {
//					client.connect(address, Integer.parseInt(port));
//				} else {
//					client.connect(address);
//				}
//				flag = client.isConnected();
//				client.login(user, password);
//				client.enterLocalPassiveMode();
//				FTPFile[] files = client.listFiles("/log/tuned");
//				logger.info("ReplyCode:"+client.getReplyCode());
//				logger.info("size:"+files.length);
//				OutputStream outputStream = null;
//				BufferedOutputStream bos = null;
//				File localFileF = new File("E://usrlogin.txt");
//				outputStream = new FileOutputStream(localFileF);
//				bos=new BufferedOutputStream(outputStream);
//				client.retrieveFile("/log/usrlogin.txt", bos);
//				bos.flush();
//			} else if ("sftp".equals(protocol)) {
//				JSch jsch = new JSch();
//				Session session = null;
//				if (StringUtil.isEmpty(port)) {// 连接服务器，采用默认端口
//					session = jsch.getSession(user, host);
//				} else {// 采用指定的端口连接服务器
//					session = jsch.getSession(user, host, Integer.parseInt(port));
//				}
//				// 设置登陆主机的密码
//				session.setPassword(password);// 设置密码
//				if (agent.getProtocol().toLowerCase().equals("socks5")) {
//					session.setProxy(new ProxySOCKS5(agent.getHost(), agent.getPort()));
//				} else if (agent.getProtocol().toLowerCase().equals("socks4")) {
//					session.setProxy(new ProxySOCKS4(agent.getHost(), agent.getPort()));
//				} else {
//					session.setProxy(new ProxyHTTP(agent.getHost(), agent.getPort()));
//				}
//				Properties sshConfig = new Properties();
//				sshConfig.put("StrictHostKeyChecking","no");
//				sshConfig.put("PreferredAuthentications","publickey,gssapi-with-mic,keyboard-interactive,password");
//				session.setConfig(sshConfig);
//				session.connect(timeOut);
//				flag = session.isConnected();
//				session.disconnect();
//			}
//			return flag;
//		}
//	}