package com.gump.algorithm.ftp;

import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;

/**
 * sofa ftp
 *
 * @author gumpliu
 * @create 2022-08-30 21:14
 */
public class SofaFTPClient extends FTPClient {

  @Override
  public void connect(final String host, final int port)
          throws SocketException, IOException
  {
    _hostname_ = null;
    _connect(host, port, null, 0);
  }

  // helper method to allow code to be shared with connect(String,...) methods
  private void _connect(final String host, final int port, final InetAddress localAddr, final int localPort)
          throws SocketException, IOException
  {
    _socket_ = _socketFactory_.createSocket();
    if (localAddr != null) {
      _socket_.bind(new InetSocketAddress(localAddr, localPort));
    }
    _socket_.connect(new InetSocketAddress(host, port), connectTimeout);
    _connectAction_();
  }

}
