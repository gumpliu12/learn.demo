package com.gump.algorithm.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 图表  邻接矩阵 二维数组形式方式
 * @Author gumpLiu
 * @Date 2023-01-07
 * @Version V1.0
 **/
public class GraphTwoArray {

    private List<String> vertexs;//定点
    private int[][] edges;//邻接矩阵
    private int numofEdges;//边数

    public GraphTwoArray(int vertesNumber){
        vertexs = new ArrayList<String>(vertesNumber);
        edges = new int[vertesNumber][vertesNumber];
    }

    public void insertVertex(String vertex){
        vertexs.add(vertex);
    }

    public void intsertEdge(int vertex1, int vertex2, int weight){
        edges[vertex1][vertex2] = weight;
        edges[vertex2][vertex1] = weight;
        numofEdges++;
    }

    public void dfs(){
        Map<Integer, Boolean> traversed = new HashMap<>();
        for(int i = 0; i < vertexs.size(); i++){
            if(!traversed.containsKey(i)){
                dfs(i, traversed);
            }
        }
    }

    private void dfs(int vertex,  Map<Integer, Boolean> traversed){
        if(traversed.containsKey(vertex)) return;

        traversed.put(vertex, true);

        for(int next = vertex + 1; next < vertexs.size(); next++){
            if(edges[vertex][next] != 0 && !traversed.containsKey(next)){
                dfs(next, traversed);
            }
        }
    }

}
