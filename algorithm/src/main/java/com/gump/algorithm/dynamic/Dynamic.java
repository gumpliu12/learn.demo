package com.gump.algorithm.dynamic;

import java.util.Arrays;

/**
 * @Description: 动态规划算法集
 * @Author gumpLiu
 * @Date 2021-06-07
 * @Version V1.0
 **/
public class Dynamic {

    public static void main(String [] args){

//        int[] sum1 = {1,9,3},sum2 = {6,4,6},sum3 ={1,1,5},m ={3,2,1};
//        changeDirection(3,sum1, sum2, sum3, m);
//        findNiuNiu2("niuniniu");
//        int[][] nums ={{1,100},{2,100},{5,100}};
//        changeMoney(3,20, nums);
        int [] nums = {51,90,28,59,76,47,9,39,3,96,36,31,81,6,45,46,78,94,27,89,20,66,76,94,27,77,70,51,99,81,88,44,54,73,7,80,53,39,15,26,88,22,54,50,76,61,10,8,57,36,28,47,32,7,79,75,86,28,19,23,9,75,32,88,59,33,2,56,21,69,74,46,78,27,92,26,88,14,22,11,46,11,1,8,62,52,91,64,21,53,69,33,69,22,90,10,58,20,79,7};
        maxProfit(nums);
    }



    /**
     * 给你一个n，和一个长度为n的数组，在不同时选位置相邻的两个数的基础上，
     * 求该序列的最大子序列和（挑选出的子序列可以为空）。
     * 4 4 8 5 7 1 2 6 9 3 4
     * 4 4 12 12 19
     * 要不要选择第n个数，
     * dp[n] 最大子序列和：dp[n] = Max(dp[n-1], dp[n-2] + array[n])
     *
     * @param n int整型 数组的长度
     * @param array int整型一维数组 长度为n的数组
     * @return long长整型
     */
    public long subsequence (int n, int[] array) {



        return 0;
    }

    /**
     * todo 未明白
     * 
     *
     * 假定你知道某只股票每一天价格的变动。
         你最多可以同时持有一只股票。但你最多只能进行两次交易（一次买进和一次卖出记为一次交易。买进和卖出均无手续费）。
         请设计一个函数，计算你所能获得的最大收益。
     * 两次交易所能获得的最大收益
     * 最大收益即：低买，高卖。
     * 是否持有股票：如果持有：只能卖出，如果没有持有：可以买入，买入金额为负数
     * dp[n] 最大收益：
     *  dp[n] =
     *
     * @param prices int整型一维数组 股票每一天的价格
     * @return int整型
     */
    public static int maxProfit (int[] prices) {
        // write code here
        int buy1=Integer.MIN_VALUE,
            buy2=Integer.MIN_VALUE;
        int sell1=0,
            sell2=0;
        for(int p:prices){
            buy1=Math.max(buy1,(-1)*p);
            sell1=Math.max(sell1,p+buy1);
            buy2=Math.max(buy2,sell1-p);
            sell2=Math.max(sell2,p+buy2);
        }
        return sell2;
    }

    /**
     * 给定一个double类型的数组arr，其中的元素可正可负可0，返回子数组累乘的最大乘积。
     * 输入： [-2.5,4,0,3,0.5,8,-1] dp[n] = array[]
     * 返回： 12.00000 -2.5 4 0 3 1.5 12 -12
     * array[i] 是否为0 ，如为0 dp[i] = 0
     * @param arr
     * @return
     */
    public static double maxProduct(double[] arr) {

        double[][] dp = new double[arr.length][arr.length];
        double max = arr[0];
        for(int i = 0; i < arr.length; i++){
            dp[i][i] = arr[i];
            for(int j = i+1; j < arr.length; j++){
                if(arr[j] == 0.0){
                    dp[i][j] = 0.0;
                }else{
                    if(dp[i][j-1] == 0.0){
                        dp[i][j] = arr[j];
                    }else{
                        dp[i][j] = dp[i][j-1] * arr[j];
                    }
                }
               max = Math.max(Math.max(dp[i][j], arr[j]), max);
            }
        }

        return max;
    }
    /**
     * 有一种将字母编码成数字的方式：'a'->1, 'b->2', ... , 'z->26'。
     * 现在给一串数字，返回有多少种可能的译码结果
     * 解码 4512621
     * @param nums string字符串 数字串
     * @return int整型
     */
    public static int decod (String nums) {
        // write code here
        if(nums == null || nums.equals("")) return 0;
        int[] dp = new int[nums.length()];
        int pre = nums.charAt(0) - '0';
        if (pre == 0) return 0;
        dp[0] = 1;
        for(int i = 1; i < nums.length(); i++){
            int current = nums.charAt(i) - '0';
            if(current != 0){
                dp[i] = dp[i-1];
            }

            int sum = pre * 10 + current;

            if(sum >= 10 && sum <=26){
                if(i == 1){
                    dp[i] += 1;
                }else{
                    dp[i] += dp[i-2];
                }
            }
            pre = current;
        }

        return dp[nums.length() - 1];
    }


    /**
     * 给定两个字符串str1和str2，输出两个字符串的最长公共子序列。
     * 如果最长公共子序列为空，则返回"-1"。目前给出的数据，仅仅会存在一个最长的公共子序列
     * 输入： "1A2C3D4B56","B1D23A456A"
     * 返回值："123456"
     * longest common subsequence
     * @param s1 string字符串 the string
     * @param s2 string字符串 the string
     * @return string字符串
     */
    public static String LCS2 (String s1, String s2) {
        // write code here
        if(s1== null || s1.length() == 0
                || s1== null || s1.length() == 0) return "";

        int[][] dp = new int[s1.length()+1][s2.length()+1];


        for(int i = 0; i < s1.length(); i++){
            for(int j = 0; j < s2.length(); j++){
                if(s1.charAt(i) == s2.charAt(j)){
                    dp[i+1][j+1] = dp[i][j] + 1;
                }else{
                    dp[i+1][j+1] = Math.max(dp[i+1][j], dp[i][j+1]);
                }
            }
        }
        StringBuffer buffer = new StringBuffer("");
        int rLastIndex = dp.length - 1;
        int lLastIndex = dp[0].length - 1;

        int currentCount = dp[rLastIndex][lLastIndex];
        if(currentCount == 0) return "-1";

        while (rLastIndex > 0 && lLastIndex > 0){

            if(currentCount > dp[rLastIndex][lLastIndex-1]
                    && currentCount > dp[rLastIndex-1][lLastIndex]){
                buffer.insert(0, s2.charAt(lLastIndex-1));
                rLastIndex--;
                lLastIndex--;
                currentCount = dp[rLastIndex][lLastIndex];
            }else if(currentCount == dp[rLastIndex][lLastIndex-1]){
                lLastIndex--;
            }else if (currentCount == dp[rLastIndex-1][lLastIndex]){
                rLastIndex--;
            }
        }

        return buffer.toString();
    }

    /**
     * 给定一个由0和1组成的2维矩阵，返回该矩阵中最大的由1组成的正方形的面积
     * 最大正方形
     * @param matrix char字符型二维数组
     * @return int整型
     */
    public static int square (int[][] matrix) {
        if(matrix == null || matrix.length == 0) return 0;

        int[][] areas = new int[matrix.length+1][matrix[0].length+1];

        int maxArea = 0;

        for(int i = 0; i< matrix.length; i++){
            for(int j = 0; j < matrix[0].length; j++){
                if(matrix[i][j] == 1 ){
                    if(areas[i][j] == 0 || areas[i][j+1] == 0 || areas[i+1][j] == 0){
                        areas[i+1][j+1] = 1;
                    }else if (areas[i][j] ==  areas[i][j+1] &&  areas[i+1][j] == areas[i][j+1]){
                        int side = (int) Math.sqrt(areas[i][j]) + 1;
                        areas[i+1][j+1] = side * side;
                    }else {
                        int area = Math.min(Math.min(areas[i][j], areas[i][j+1]), areas[i+1][j]);
                        int side = (int) Math.sqrt(area) + 1;
                        areas[i+1][j+1] = side * side;
                    }
                    maxArea = Math.max(maxArea,  areas[i+1][j+1]);
                }
            }
        }
        for(int i = 0; i< areas.length; i++){
            for (int j = 0; j< areas[0].length; j++){
                System.out.print(areas[i][j] + ",");
            }
            System.out.println("");
        }
        return maxArea;
    }


    /**
     * 给定两个字符串str1和str2,输出两个字符串的最长公共子串
     * 题目保证str1和str2的最长公共子串存在且唯一。
     * 输入："1AB2345CD","12345EF"
     * 返回：2345
     *
     * @param str1
     * @param str2
     * @return
     */
    public static String LCS (String str1, String str2) {
        // write code here
        if(str1== null || str1.length() == 0
            || str2== null || str2.length() == 0) return "";
        int[][] dp = new int[str1.length()+1][str2.length()+1];
        int max = 0; int index = 0;

        for(int i = 0; i < str1.length(); i++){
            for(int j = 0; j < str2.length(); j++){
                if(str1.charAt(i) == str2.charAt(j)){
                    dp[i+1][j+1] = dp[i][j] + 1;
                    if(max < dp[i+1][j+1]){
                        max = dp[i+1][j+1];
                        index = i;
                    }
                }
            }
        }

        StringBuffer buffer = new StringBuffer("");
        for(int i = index - max + 1; i <=index; i++){
            buffer.append(str1.charAt(i));
        }
        return  buffer.toString();
    }

    /**
     * 牛牛现在有一个n个数组成的数列,牛牛现在想取一个连续的子序列,并且这个子序列还必须得满足:
     *      最多只改变一个数,就可以使得这个连续的子序列是一个严格上升的子序列,
     *      牛牛想知道这个连续子序列最长的长度是多少。
     * @param nums int一维数组
     * @return int
     */
    public static int maxSubArrayLength (int[] nums) {
        // write code here

        int[] left = new int[nums.length];
        int[] right = new int[nums.length];
        left[0] = 1;
        right[nums.length -1] = 1;

        for(int i = 1; i < nums.length; i++){
            if(nums[i] > nums[i-1]){
                left[i] = left[i-1]+1;
            }else{
                left[i] = 1;
            }
        }

        for(int i = nums.length - 2; i >= 0; i--){
            if(nums[i] < nums[i+1]){
                right[i] = right[i+1] + 1;
            }else{
                right[i] = 1;
            }
        }

        int max = 1;
        for(int i = 1; i < nums.length - 1; i++){
            int left_count = left[i-1];
            int right_count = right[i+1];
            if(nums[i-1] < nums[i+1]){
                max = Math.max(max, left_count+ right_count);
            }
        }
        return max + 1;
    }

    /**
     * 给你一个长度为n的数组A。
     * A[i]表示从i这个位置开始最多能往后跳多少格.求从1开始最少需要跳几次就能到达第n个格子。
     *
     * 最少需要跳跃几次能跳到末尾
     * @param n int整型 数组A的长度
     * @param A int整型一维数组 数组A
     * @return int整型
     */
    public static int jump (int n, int[] A) {
        int[] dp = new int[n];
        Arrays.fill(dp, Integer.MAX_VALUE);
        dp[0] = 0;
        for(int i = 0; i < n; i++) {
            int jump = A[i];
            int end = i + jump;
            for(int j = i + 1; j <= end && j < n; j++) {
                dp[j] = Math.min(dp[j], dp[i] + 1);
            }
            if(i + jump >= n) break;
        }

        return dp[n - 1];
    }

    /**
     * 贪心 算法
     * @param n
     * @param A
     * @return
     */
    public static int jump1 (int n, int[] A) {
        int max=0;
        int end=0;
        int step=0;
        for(int i=0;i<n-1;i++){
            max=Math.max(max,i+A[i]);
            if(end + A[i] >= n){
                step++;
                break;
            }
            if(i==end){
                end=max;
                step++;
            }
        }
        return step;
    }

    /**
     *
     * 定义一种新货币，有n(n<=50)种不同的币值，其中币值为 value(value<=50) 的有 w(w<=20) 个。
     * 现在你有 x(x<=100) 元，但是你想将 x 元换成若干零钱，请问有多少种换钱的方案？
     *
     * @param n int整型 ：牛币值种类数
     * @param x int整型 ：牛妹拥有的钱数
     * @param a int整型二维数组 ：第二个vector中的第一列表示币值，第二列表示牛牛拥有币值的个数
     * @return int整型
     */
    public static int changeMoney (int n, int x, int[][] a) {
        // write code here
        int[][] dp = new int[n+1][x+1];
        dp[0][0]=1;
        for(int i = 1; i <= n; i++) {
            int money=a[i-1][0];
            for (int j = 0; j <= x; j++) {
                for (int k = 0; k <= a[i - 1][1]; k++) {
                    if(j-k*money>=0)
                        dp[i][j]+=dp[i-1][j-k*money];
                    else
                        break;
                }
            }
        }
        return dp[n][x];
    }

    /**
     * 给出一个字符串S，牛牛想知道这个字符串有多少个子序列等于"niuniu"
     * 子序列可以通过在原串上删除任意个字符(包括0个字符和全部字符)得到。
     * 为了防止答案过大，答案对1e9+7取模
     * 输入： "niuniniu"
     * 返回值： 3
     * 说明：
     *      删除第4，5个字符可以得到"niuniu"
     *      删除第5，6个字符可以得到"niuniu"
     *      删除第6，7个字符可以得到"niuniu"
     *
     * @param s string字符串
     * @return int整型
     */
    public static int findNiuNiu (String s) {
        String paramWork = "niuniu";
        int len= paramWork.length();
        int[]arr=new int[len+1];
        arr[0]=1;
        for (int i=1;i<s.length()+1;i++) {
            for(int j=Math.min(i,len);j>0;j--){
                if(s.charAt(i-1)== paramWork.charAt(j-1)){
                    arr[j]=arr[j]+arr[j-1];
                }
            }
        }

        return arr[len];

    }

    /**
     * 思路是：判断当前字符是否相等，如相等写出表达，不能写出表达式。
     *       画出二维数组
     * 使用二维数组
     *  paramwork[m] == s[n]   dp[m][n] = dp[m][n-1] + dp[m-1][n-1]
     *  paramwork[m] != s[n]   dp[m][n] = dp[m][n-1]
     *
     * @param s
     * @return
     */
    public static int findNiuNiu2 (String s) {
        String paramWork = "niuniu";
        int[][] nums = new int[paramWork.length()+1][s.length()+1];
        for(int i = 0; i<=s.length(); i++){
            nums[0][i] = 1;
        }
        for(int i = 1; i<=paramWork.length(); i++){
            for(int j = 1; j<=s.length(); j++){
                if(paramWork.charAt(i-1) == s.charAt(j-1)){
                    nums[i][j] = nums[i][j-1] + nums[i-1][j-1];
                }else{
                    nums[i][j] = nums[i][j-1];
                }
            }
        }
        return nums[paramWork.length()][s.length()];

    }


    /**
     * 牛牛准备在一个3行n列的跑道上跑步。一开始牛牛可以自己选择位于(1,1)还是(2,1)还是(3,1)。

     跑道的每一格都有一些金币，当牛牛跑到一个格子，他会获得这个格子的所有金币。

     当牛牛位于第i行第j列时，他可以的下一步最多可能有三种选择：

     1. 不花费金币跑到第i行第j+1列
     2. 花费mj ​的金币跑到第i-1行第j+1列（如果i=1则不可以这么跑）。
     3. 花费mj 的金币跑到第i+1行第j+1列（如果i=3则不可以这么跑）。
     （牛牛是一个富豪，本身带了很多的金币，所以你不用担心他钱不够用）
       现在告诉你所有格子的金币数量和每一列的金币花费，牛牛想知道他跑到第n列最多可以赚得多少金币（赚得金币=获得金币-消耗金币）
     输入：3,[1,9,3],[6,4,6],[1,1,5],[3,2,1]
     返回值：16
     说明：
         一开始牛牛选择位于第2行第1列，拿到6个金币。然后牛牛花3金币到第1行的2列拿到9个金币，
         最后牛牛花费2金币到第2行第3列。总共获得21金币，消耗5金币。赚得16金币。


     * @param n
     * @param a1
     * @param a2
     * @param a3
     * @param m
     * @return
     */
    public static int changeDirection (int n, int[] a1, int[] a2, int[] a3, int[] m) {
        // write code here
        if(n <=0) return 0;

        int[][] dp = new int[3][n];
        for(int i = 0; i<3; i++){
            if(i == 0) dp[i][0] = a1[0];
            if(i == 1) dp[i][0] = a2[0];
            if(i == 2) dp[i][0] = a3[0];

        }

        for(int i = 1; i < n; i++){
            for(int j =0; j<3; j++){
                if(j == 0){
                    dp[j][i] = Math.max(dp[j][i-1] + a1[i],dp[j+1][i-1]+a1[i] - m[i-1]);
                }else if(j == 1){
                    int value = Math.max(dp[j-1][i-1] + a2[i] - m[i-1],dp[j][i-1]+a2[i]);
                    dp[j][i] = Math.max(dp[j+1][i-1] + a2[i] - m[i-1], value);
                }else if(j == 2){
                    dp[j][i] = Math.max(dp[j-1][i-1] + a3[i] - m[i-1], dp[j][i-1] + a3[i]);
                }

            }
        }

        return Math.max(Math.max(dp[0][n-1], dp[1][n-1]),dp[2][n-1]);
    }

    /**
     * 给定数组arr，arr中所有的值都为正整数且不重复。每个值代表一种面值的货币，每种面值的货币可以使用任意张，
     * 再给定一个aim，代表要找的钱数，求组成aim的最少货币数。如果无解，请返回-1.
     * 要求：时间复杂度O(n×aim)，空间复杂度O(n)。
     * 输入：[5,2,3],20
     * 输出：4
     * @param arr int整型一维数组 the array
     * @param aim int整型 the target
     * @return int整型
     */
    public static int minMoney (int[] arr, int aim) {
        // write code here
        int[] aims = new int[aim+1];
        for(int i = 0; i<= aim; i++){
            aims[i] = Integer.MAX_VALUE - 1;
        }

        for(int i = 1; i<=aim;i++){
            int minValue = Integer.MAX_VALUE - 1;
            for(int j = 0; j<arr.length;j++){
                if(arr[j] > i) continue;

                if(arr[j] == i){
                    minValue = 1;
                    break;
                }else{
                    minValue = Math.min(aims[i - arr[j]] + 1, minValue);
                }
            }
            aims[i] = minValue;
        }

        return aims[aim] == Integer.MAX_VALUE-1 ? -1 : aims[aim];

    }


    /**
     * 一个机器人在m×n大小的地图的左上角（起点）。
     * 机器人每次向下或向右移动。机器人要到达地图的右下角（终点）。
     * 可以有多少种不同的路径从起点走到终点？
     * @param m
     * @param n
     * @return
     */
    public  static int uniquePaths (int m, int n) {
        // write code here
        int[][] dp = new int[m+1][n+1];
        for(int i = 0; i<=n; i++)
            dp[0][i] = 1;
        for(int i = 0; i<=m; i++)
            dp[i][0] = 1;

        for(int i = 1; i<m; i++){
            for(int j = 1; j <n; j++){
                dp[i][j] = dp[i-1][j] + dp[i][j-1] ;
            }
        }

        return dp[m-1][n-1];
    }

    /**
     * 最大子串是要找出由数组成的一维数组中和最大的连续子串。
     * 比如{5,-3,4,2}的最大子串就是 {5,-3,4,2}，它的和是8,达到最大；
     * 而 {5,-6,4,2}的最大子串是{4,2}，它的和是6。
     *
     * dp[n] dp[n-1] < 0, dp[n]=array[n]
     *                    dp[n]=dp[n-1] + array[n]
     *         max =
     */
}
