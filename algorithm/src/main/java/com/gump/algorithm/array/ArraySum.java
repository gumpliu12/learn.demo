package com.gump.algorithm.array;

import java.util.*;

/**
 * 计算两数之和
 *
 * @author gumpliu
 * @create 2021-04-25 22:34
 */
public class ArraySum {

  public static void main(String[] args){

    int[]  num = {1,2};
//    ArrayList<ArrayList<Integer>> list = threeSum3(num);
//    System.out.println(list);
    findUnsortedSubarray(num);
    System.out.println(String.format("http://127.0.0.1:8888/portal/refushToken/%s/%s", "ss", "sss"));

  }

  /**
   * 给你一个整数数组 nums ，你需要找出一个 连续子数组 ，如果对这个子数组进行升序排序，那么整个数组都会变为升序排序。
   * 请你找出符合题意的 最短 子数组，并输出它的长度。
   *
   * @param nums
   * @return
   */
  public static int findUnsortedSubarray(int[] nums) {
    if(nums == null || nums.length == 0) return 0;

    int left = -1, right = -1, min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;

    for(int i = 0; i < nums.length; i++){

      if(max > nums[i]) right = i;
      else max = nums[i];

      if(min < nums[nums.length - 1 - i]) left = nums.length - 1 - i;
      else min = nums[nums.length - 1 - i];
    }

    return right == -1 ? 0 : right - left + 1;

  }

  /**
   * 给你一个整数数组 nums 和一个整数 k ，请你统计并返回 该数组中和为 k 的连续子数组的个数 。
   * @param nums
   * @param k
   * @return
   */
  public static int subarraySum(int[] nums, int k) {
    if(nums == null || nums.length == 0) return 0;
    int count = 0, pre = 0;
    Map<Integer, Integer> map = new HashMap<>();
    map.put(0,1);
    for(int i = 0; i < nums.length; i++){
      pre += nums[i];
      if(map.containsKey(pre - k)){
        count+= map.get(pre-k);
      }
      map.put(pre, map.getOrDefault(pre, 0) + 1);
    }
    return count;
  }
  /**
   *
   * @param numbers int整型一维数组
   * @param target int整型
   * @return int整型一维数组
   */
  public int[] twoSum (int[] numbers, int target) {
    Map<Integer, Integer> map = new HashMap();
    int[] revalue = new int[2];
    for(int i = 0; i< numbers.length; i++){
      if(map.containsKey(target - numbers[i])){
        revalue[0] =  i + 1;
        revalue[1] = map.get(target - numbers[i]);
        break;

      }

      map.put(numbers[i], i + 1);
    }

    return revalue;

  }

  /**
   * 给出一个有n个元素的数组S，S中是否有元素a,b,c满足a+b+c=0？找出数组S中所有满足条件的三元组。
   * 注意：
   *      三元组（a、b、c）中的元素必须按非降序排列。（即a≤b≤c）
   *      解集中不能包含重复的三元组。
   * @param num
   * @return
   */
  public static ArrayList<ArrayList<Integer>> threeSum(int[] num) {

    ArrayList<ArrayList<Integer>> list = new ArrayList();

    if(num == null || num.length < 3) return list;

    Arrays.sort(num);

    if(num[num.length-1] < 0 || num[0] > 0) return list;

    int maxZeroFirstIndex = 0;
    for(int i = 0; i < num.length; i++){
      if(num[i] >= 0){
        maxZeroFirstIndex = i + 1;
        break;
      }
    }

    int preFistNum = 0 , preSendNum = 0, preThridNum = 0;

    for(int i = 0; i < maxZeroFirstIndex; i++){
      int firstNum = num[i];

      for(int j = i+1; j< num.length - 1; j++){
        int sencondNum = num[j];
        if((firstNum + sencondNum) > 0) break;

        for(int g = (j+1)>= maxZeroFirstIndex ? j+1 : maxZeroFirstIndex; g < num.length; g++){

          int count = firstNum + sencondNum + num[g];

          if(count > 0 ) break;
          else if(count == 0){
            ArrayList<Integer> arrayList = new ArrayList<Integer>();

            if(list.isEmpty()){
              preFistNum = firstNum;
              preSendNum = sencondNum;
              preThridNum = num[g];
            }else{
              if(preFistNum == firstNum &&
                      preSendNum == sencondNum &&
                      preThridNum == num[g]){
                continue;
              }
            }

            arrayList.add(firstNum);
            arrayList.add(sencondNum);
            arrayList.add(num[g]);
            list.add(arrayList);
          }
        }
      }
    }

    return list;

  }

  public static ArrayList<ArrayList<Integer>> threeSum2(int[] num) {
    ArrayList<ArrayList<Integer>> res=new ArrayList<ArrayList<Integer>>();

    if(num==null || num.length<3){
      return res;
    }
    Arrays.sort(num);// 排序
    for(int i=0;i<num.length-2;i++){
      if(num[i]>0){
        break;// 如果当前数字大于0，则三数之和一定大于0，所以结束循环
      }
      if(i>0 && num[i]==num[i-1]){
        continue;// 去重
      }
      int L=i+1;
      int R=num.length-1;

      while(L<R){
        int sum=num[i]+num[L]+num[R];
        if(sum==0){
          ArrayList<Integer> list=new ArrayList<Integer>();
          list.add(num[i]);
          list.add(num[L]);
          list.add(num[R]);
          res.add(list);

          while(L<R && num[L]==num[L+1]){
            L++;
          }
          while(L<R && num[R]==num[R-1]){
            R--;
          }
          L++;
          R--;
        }
        else if(sum>0){
          R--;
        }
        else if(sum<0){
          L++;
        }
      }
    }
    return res;
  }


  public static ArrayList<ArrayList<Integer>> threeSum3(int[] num) {
    ArrayList<ArrayList<Integer>> res=new ArrayList<ArrayList<Integer>>();
    if(num==null || num.length<3){
      return res;
    }
    Arrays.sort(num);// 排序
    HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
    int maxZeroFirstIndex = 0;
    for(int i = 0; i < num.length; i++){
      if(num[i] >= 0){
        maxZeroFirstIndex = i + 1;
        break;
      }
    }

    if(num[num.length-1] < 0 || num[0] > 0) return res;

    if(num[0] == num[num.length - 1]){
      ArrayList<Integer> list=new ArrayList<Integer>();
      list.add(num[0]);
      list.add(num[0]);
      list.add(num[0]);
      res.add(list);
      return res;
    }

    for(int i = maxZeroFirstIndex - 1; i < num.length; i++){
      map.put(num[i], num[i]);
    }

    for(int i = 0; i < maxZeroFirstIndex; i++){
      int firstNum = num[i];
      if(i>0 && num[i]==num[i-1]){
        continue;// 去重
      }

      for(int j = i+1; j< num.length; j++){

        int sencondNum = num[j];
        if(j>0 && num[j]==num[j-1]){
          continue;// 去重
        }
        if((firstNum + sencondNum) > 0) break;

        int thirdSum = 0 - (firstNum + sencondNum);

        if(thirdSum < sencondNum) break;

        if(map.containsKey(thirdSum)){
          ArrayList<Integer> arrayList = new ArrayList<Integer>();
          arrayList.add(firstNum);
          arrayList.add(sencondNum);
          arrayList.add(thirdSum);
          res.add(arrayList);
        }
      }
    }

    return res;
  }


}
