package com.gump.algorithm.array;

import java.util.*;

/**
 * @Description: 数组结构，求最值
 * @Author gumpLiu
 * @Date 2021-05-06
 * @Version V1.0
 **/
public class ArrayExtremeValue {

    public static void main(String[] args) {
//        int[][] nums = {{1,2},{3,4}};
//        int[][] nums =  {{2,29,20,26,16,28},{12,27,9,25,13,21},{32,33,32,2,28,14},{13,14,32,27,22,26},{33,1,20,7,21,7},{4,24,1,6,32,34}};
//        int[][] nums = {{5,1,9,11},{2,4,8,10},{13,3,6,7},{15,14,12,16}};
        int[] nums =  {100,4,200,1,3,2};
        longestConsecutive(nums);
    }

    public int[] productExceptSelf(int[] nums) {
        if(nums == null) return null;
        int[] result = new int[nums.length];
        for(int i = 1; i < nums.length; i++){
            result[i] = result[i-1] * nums[i-1];
        }

        int r = 1;
        for(int j = nums.length - 1; j >= 0; j --){
            result[j] = result[j] * r;
            r *= nums[j];
        }


        return result;
    }

    public  int numIslands(char[][] grid) {
        if(grid == null || grid.length == 0) return 0;
        int count = 0;
        Queue<HL> queue = new LinkedList<HL>();
        for(int i = 0; i < grid.length; i++){
            for(int j = 0; j < grid[0].length; j++){
                if(grid[i][j] == '1'){
                    queue.offer(new HL(i,j));
                    count++;
                    grid[i][j] = '0';
                    while (!queue.isEmpty()){
                        HL hl = queue.poll();
                        if(hl.h - 1 >= 0 && grid[hl.h-1][hl.l] == '1'){
                            queue.offer(new HL(hl.h-1, hl.l));
                            grid[hl.h-1][hl.l] = '0';
                        }
                        if(hl.h + 1 < grid.length && grid[hl.h+1][hl.l] == '1'){
                            queue.offer(new HL(hl.h+1, hl.l));
                            grid[hl.h+1][hl.l] = '0';
                        }

                        if(hl.l - 1 >= 0 && grid[hl.h][hl.l-1] == '1'){
                            queue.offer(new HL(hl.h, hl.l-1));
                            grid[hl.h][hl.l-1] = '0';
                        }
                        if(hl.h + 1 < grid[0].length && grid[hl.h][hl.l+1] == '1'){
                            queue.offer(new HL(hl.h, hl.l+1));
                            grid[hl.h][hl.l+1] = '0';
                        }
                    }
                }
            }
        }
        return count;
    }
    class HL{
        public int h;
        public int l;
        public HL(int h, int l){
            this.h = h;
            this.l = l;
        }
    }


    /**
     * 给定一个 m x n 二维字符网格 board 和一个字符串单词 word 。如果 word 存在于网格中，返回 true ；否则，返回 false 。
     * 单词必须按照字母顺序，通过相邻的单元格内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母不允许被重复使用。
     */
    public boolean exist(char[][] board, String word) {
        if(word == null ||word.equals("")) return false;
        char[] chars = word.toCharArray();
        Map<String, Character> map = new HashMap<String, Character>();
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[0].length; j++){
                int h = i, l = j;
                for(int len = 0; len < chars.length; len++){
                    if (board[h][l] != chars[len]){
                        break;
                    }else{

                    }

                }
            }
        }
        return false;
    }

    /**
     * class Solution:
     *     def maxProduct(self, nums: List[int]) -> int:
     *         reverse_nums = nums[::-1]
     *         for i in range(1, len(nums)):
     *             nums[i] *= nums[i - 1] or 1
     *             reverse_nums[i] *= reverse_nums[i - 1] or 1
     *         return
     */
    public int maxProduct(int[] nums) {
        int[] rnums = new int[nums.length];
        for(int i = 0; i < nums.length; i++){
            rnums[nums.length -1 - i] = nums[i];
        }
        for(int i = 1; i < nums.length; i++){
            nums[i] = nums[i-1] == 0 ? nums[i] : nums[i-1] * nums[i];
            rnums[i] = rnums[i-1] == 0 ? rnums[i] : rnums[i-1] * rnums[i];
        }
        int maxValue = Integer.MIN_VALUE;
        for(int i = 0; i < nums.length; i++){
            maxValue = Math.max(maxValue, Math.max(nums[i], rnums[i]));
        }
        return maxValue;

    }

    public int maximalSquare(char[][] matrix) {

        if(matrix == null || matrix.length == 0) return 0;
        int[][] dp = new int[matrix.length][matrix[0].length];
        int maxvalue = 0;
        for(int i = 0; i < matrix.length; i++){
            if(matrix[i][0] == '1') {
                dp[i][0] = 1;
                maxvalue = 1;
            }
        }
        for(int j = 0; j < matrix[0].length; j++){
            if(matrix[0][j] == '1') {
                dp[0][j] = 1;
                maxvalue = 1;
            }
        }

        for(int i = 1; i < matrix.length; i++){
            for(int j = 1; j < matrix[0].length; j++){
                if(matrix[i][j] == '1'){
                    dp[i][j] = Math.min(Math.min(dp[i-1][j], dp[i][j-1]), dp[i-1][j-1]) + 1;
                    maxvalue = Math.max(maxvalue, dp[i][j]);
                }
            }
        }
        return maxvalue * maxvalue;
    }

    public static int longestConsecutive(int[] nums) {
        if (nums.length == 0) return 0;

        // 首次遍历，与邻居结盟
        UnionFind uf = new UnionFind(nums);
        for (int v : nums)
            uf.union(v, v + 1); // uf.union() 结盟

        // 二次遍历，记录领队距离
        int max = 1;
        for (int v : nums)
            max = Math.max(max, uf.find(v) - v + 1); // uf.find() 查找领队
        return max;
    }
    static class UnionFind {
        private int count;
        private Map<Integer, Integer> parent; // (curr, leader)

        UnionFind(int[] arr) {
            parent = new HashMap<Integer,Integer>();
            for (int v : arr)
                parent.put(v, v); // 初始时，各自为战，自己是自己的领队

            count = parent.size(); // 而非 arr.length，因可能存在同 key 的情况
            // 感谢 [@icdd](/u/icdd/) 同学的指正
        }

        // 结盟
        void union(int p, int q) {
            // 不只是 p 与 q 结盟，而是整个 p 所在队伍 与 q 所在队伍结盟
            // 结盟需各领队出面，而不是小弟出面
            Integer rootP = find(p), rootQ = find(q);
            if (rootP == rootQ) return;
            if (rootP == null || rootQ == null) return;

            // 结盟
            parent.put(rootP, rootQ); // 谁大听谁
            // 应取 max，而本题已明确 p < q 才可这么写
            // 当前写法有损封装性，算法题可不纠结

            count--;
        }

        // 查找领队
        Integer find(int p) {
            if (!parent.containsKey(p))
                return null;

            // 递归向上找领队
            int root = p;
            while (root != parent.get(root))
                root = parent.get(root);

            // 路径压缩：扁平化管理，避免日后找领队层级过深
            while (p != parent.get(p)) {
                int curr = p;
                p = parent.get(p);
                parent.put(curr, root);
            }

            return root;
        }
    }


    /**
     * 旋转图片
     *
     * @param matrix
     */
    public static void rotate(int[][] matrix) {
        int len = matrix.length;
        if(len == 1) return;
        int start = 0, end = len-1;
        while (start < end){
            int[] snums = Arrays.copyOfRange(matrix[start],start, start + end - start + 1);
            int[] enums = Arrays.copyOfRange(matrix[end],start, start + end - start + 1);
            for(int i = start, j = end; i <= end || j >=start; i++,j--){
                matrix[start][j] = matrix[i][start];
            }
            for(int i = end, j = start; i >= start || j <=end; i--,j++){
                matrix[end][j] = matrix[i][end];
            }
            for(int i = start, j = 0; i <= end || j < snums.length; i++,j++){
                matrix[i][end] = snums[j];
            }
            for(int i = start, j = 0; i <= end || j < enums.length; i++,j++){
                matrix[i][start] = enums[j];
            }
            start++;
            end--;
        }
    }

    public static int numDecodings(String s) {
        if(s.length() > 0 && s.charAt(0) == '0') return 0;
        if(s.length() == 1) return 1;
        int[] dp = new int[s.length()];
        dp[0] = 1;
        dp[1] = s.charAt(1) == '0' || Integer.valueOf(s.substring(0,2)) > 26 ? 1 : 2;

        for(int i = 2; i < s.length(); i++){
            if(s.charAt(i) == '0'){
                if(Integer.valueOf(s.substring(i-1, i+1)) > 26){
                    return 0;
                }else{
                    dp[i] = dp[i-2];
                }
            }else{
                if(s.charAt(i-1) == '0' ){
                    dp[i] = dp[i-3];
                }else if(Integer.valueOf(s.substring(i-1, i+1)) > 26){
                    dp[i] = dp[i-1];
                }else{
                    dp[i] = dp[i-2] + dp[i-1];
                }
            }
        }

        return dp[s.length()-1];
    }


    public static int minPathSum1(int[][] grid) {

        int[] dp  = new int [grid[0].length];
        for(int i = 0; i < grid[0].length; i++){
            dp[i] = i == 0 ? grid[0][i] : grid[0][i] + dp[i-1];
        }
        for(int i = 1; i < grid.length; i++){
            for(int j = 0; j < grid[0].length; j++){
                dp[j] = j == 0 ? dp[j] + grid[i][j] : Math.min(dp[j], dp[j-1]) + grid[i][j];
            }
        }

        return dp[grid.length-1];
    }

    public static int majorityElement(int[] nums) {
        Arrays.sort(nums);
        int cur = nums[0];
        int num = 1;
        for(int i = 1; i < nums.length; i++){
            if(cur == nums[i]){
                num++;
            }else{
                if(num > (nums.length/2)){
                    return cur;
                }
                cur = nums[i];
                num = 1;
            }
        }
        if(num > (nums.length/2)){
            return cur;
        }
        return -1;
    }

    public static boolean minDistance(int  x) {
        if(x < 0) return false;
        if(x > 10) return true;
        String str = x + "";
        int left = 0; int rigth = str.length() -1;
        while(left < rigth){
            if(str.charAt(left) != str.charAt(rigth)){
                return false;
            }
            left++;
            rigth--;
        }
        return true;
    }

    /**
     * 给定一个 n * m 的矩阵 a，从左上角开始每次只能向右或者向下走，
     * 最后到达右下角的位置，路径上所有的数字累加起来就是路径和，输出所有的路径中最小的路径和。
     * @param matrix int整型二维数组 the matrix
     * @return int整型
     */
    public static int minPathSum (int[][] matrix) {
        if(matrix == null || matrix.length == 0) return 0;

        int[][] mins = new int[matrix.length][matrix[0].length];
        mins[0][0] = matrix[0][0];
        for(int i = 1; i < matrix.length; i++){
            mins[i][0] = mins[i-1][0] + matrix[i][0];
        }

        for(int j = 1; j < matrix[0].length; j++){
            mins[0][j] = mins[0][j-1] + matrix[0][j];
        }

        for(int i = 1; i < matrix.length; i++){
            for(int j = 1; j < matrix[i].length; j++){
                mins[i][j] = Math.min(mins[i][j-1],  mins[i-1][j]) + matrix[i][j];
            }

        }

        return  mins[matrix.length-1][matrix[0].length-1];

    }

}
