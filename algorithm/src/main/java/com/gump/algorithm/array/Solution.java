package com.gump.algorithm.array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Solution {

    public static void main(String[] args) {

        int[] num = {6,7,8,9,1,2,3,4,5};
        search(num, 8);


    }


    static class Node implements Comparator<Node> {
        public int value;
        public int idx;

        public Node() {

        }
        //value权值大小，arraysIdx在哪个数组里，idx在该数组的哪个位置> >
        public Node(int value, int idx) {
            this.value = value;
            this.idx = idx;
        }

        public int compare(Node n1, Node n2) {
            if(n1.value < n2.value) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    static Comparator<Node> cNode = new Comparator<Node>() {
        public int compare(Node o1, Node o2) {
            return o1.value - o2.value;
        }

    };
    public static int[] subarraySumClosest(int[] nums) {

        // write your code here
        //前缀和数组，并记录下标位置
        ArrayList<Node> sums = new ArrayList<Node>();
        for(int i = 0; i < nums.length; i++) {
            if(i == 0) {
                sums.add(new Node(nums[i], 0));
            } else {
                sums.add(new Node(nums[i] + sums.get(i - 1).value, i));
            }
        }
        Collections.sort(sums, cNode);
        //维护最小的绝对值以及位置
        int mn = 2147483647;
        int ans1 = 0, ans2 = 1;
        for(int i = 0; i < sums.size(); i++) {
            if(mn >= Math.abs(sums.get(i).value)) {
                //[0,idx] 这段子数组的和
                mn = Math.abs(sums.get(i).value);
                ans1 = 0;
                ans2 = sums.get(i).idx;
            }
            if(i > 0) {
                // [lastidx+1,nowidx]这段子数组的和
                int lastidx = sums.get(i - 1).idx;
                int nowidx = sums.get(i).idx;
                int lastsum = sums.get(i - 1).value;
                int nowsum = sums.get(i).value;
                if(mn >= Math.abs(nowsum - lastsum)) {
                    mn = Math.abs(nowsum - lastsum);
                    ans1 = Math.min(lastidx, nowidx) + 1;
                    ans2 = Math.max(lastidx, nowidx);
                }
            }
        }
        int []ans = new int[2];
        ans[0] = ans1;
        ans[1] = ans2;
        return ans;
    }

    /**
     * 给定一个整形数组arr，已知其中所有的值都是非负的，将这个数组看作一个容器，请返回容器能装多少水。
     * 输入   3 1 2 5 2 4
     * 输出   5
     *
     * max water
     * @param arr int整型一维数组 the array
     * @return long长整型
     */
    public static long maxWater (int[] arr) {

        if(arr == null ||arr.length <=2) return 0;

        //正向求装水量
        int currMin = arr[0]; int currMinIndex = 0; long writerValue = 0;
        for(int i = 1; i < arr.length; i++ ){
            if(arr[i] < currMin) continue;

            if(arr[i] >= currMin && i - currMinIndex >1){
                for(int j = currMinIndex+1; j < i; j++){
                    writerValue += currMin - arr[j];
                }
            }
            currMin = arr[i];
            currMinIndex = i;
        }
        //反向求装水量
        currMin = arr[arr.length - 1]; currMinIndex = arr.length - 1;
        for(int i = arr.length - 2; i >= 0; i-- ){
            if(arr[i] < currMin) continue;

            if(arr[i] > currMin && currMinIndex - i >1){
                for(int j = i+1; j < currMinIndex; j++){
                    writerValue += currMin - arr[j];
                }
            }
            currMin = arr[i];
            currMinIndex = i;
        }

        return writerValue;
    }

    /**
     * 给定一个整数数组nums，按升序排序，数组中的元素各不相同。
     * nums数组在传递给search函数之前，会在预先未知的某个下标
     * t（0 <= t <= nums.length-1）上进行旋转，让数组变为[nums[t], nums[t+1], ..., nums[nums.length-1], nums[0], nums[1], ..., nums[t-1]]。
     * 比如，数组[0,2,4,6,8,10]在下标2处旋转之后变为[6,8,10,0,2,4]
     * 现在给定一个旋转后的数组nums和一个整数target，请你查找这个数组是不是存在这个target，如果存在，那么返回它的下标，如果不存在，返回-1
     *
     *
     * @param nums int整型一维数组
     * @param target int整型
     * @return int整型
     */
    public static int search (int[] nums, int target) {
        // write code here
        if(nums == null || nums.length == 0) return -1;

        if(nums.length == 1) return nums[0] == target ? 0 : -1;

        int rotateIndex = rotateIndex(nums);

        if(nums[rotateIndex] == target) return rotateIndex;

        if(nums[rotateIndex] < target || nums[rotateIndex+1] > target) return -1;

        int start = 0, end = nums.length - 1;
        if(nums[rotateIndex] > target && nums[0] < target){
            end = rotateIndex - 1;
        }else {
            start = rotateIndex + 1;
        }

        while (start <= end){
            int mod = start + ((end - start) >> 1);
            if(nums[mod] > target){
                end = mod-1;
            }else if(nums[mod] < target){
                start = mod + 1;
            }else{
                return mod;
            }
        }
        return -1;
    }

    public int search1 (int[] nums, int target) {
        // write code here
        int left=0;
        int right=nums.length-1;
        int mid=(left+right)>>1;
        while(left<right){
            if(nums[mid]==target){
                return mid;
            }
            if(nums[mid]<target&&target<=nums[right]){
                left=mid+1;
            }else{
                right=mid;
            }
            mid=(left+right)>>1;
        }
        return nums[mid]==target?mid:-1;
    }

    //二分查询，获取旋转点
    public static int rotateIndex(int[] nums){

        //二分查询旋转点
        int start = 0, end = nums.length - 1;

        while (start <= end){
            int mod = start + ((end - start) >> 1);
            if(nums[start] > nums[mod]){
                end = mod-1;
            }else if(nums[mod] > nums[end]){
                start = mod + 1;
            }else{
                return start - 1 == -1 ? end : start -1;
            }
        }

        return -1;
    }


}