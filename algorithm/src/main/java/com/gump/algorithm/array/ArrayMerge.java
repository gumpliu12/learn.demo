package com.gump.algorithm.array;

/**
 * @Description: 数组合并
 * @Author gumpLiu
 * @Date 2021-05-06
 * @Version V1.0
 **/
public class ArrayMerge {

    public static void main(String[] args) {
        int[] A = new int[8];
        int[] B = {1};
        A[0] = 2;
        merge(A, 1, B, 1);
    }

    /**
     * 给出两个有序的整数数组A和B，请将数组B合并到数组A中，变成一个有序的数组
     * 注意：
     * 可以假设A数组有足够的空间存放B数组的元素，A和B中初始的元素数目分别为M和N
     * @param A
     * @param m
     * @param B
     * @param n
     */
    public static void merge(int A[], int m, int B[], int n) {
        if(B == null || n == 0) return;

        if(m == 0){
            for(int i = 0; i< n; i++){
                A[i] = B[i];
            }
        }else {
            while (m > 0 && n > 0){
                if(A[m-1] > B[n-1]){
                    A[m+n-1] = A[m-1];
                    --m;
                }else{
                    A[m+n-1] = B[n-1];
                    --n;
                }
            }

            if(n>0){
                while (n > 0){
                    A[n-1] = B[n-1];
                    --n;
                }
            }

        }
    }
}
