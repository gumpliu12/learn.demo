package com.gump.algorithm.array;

import java.util.*;

/**
 * 二维数组，查找指定值
 *
 * @author gumpliu
 * @create 2021-04-22 21:59
 */
public class ArrayFind {

  public static void main(String [] args){
//    int [] num = {2,3,4,2,6,2,5,1};
//    maxInWindows(num, 3);
    PriorityQueue<Integer> queue = new PriorityQueue((o1, o2) ->{ return (Integer) o1 - (Integer)o2;});
    queue.offer(10);
    queue.offer(9);
    queue.offer(8);
    queue.offer(7);
    queue.offer(6);
    queue.offer(5);
    queue.offer(4);
    queue.offer(3);
    queue.poll();
    System.out.println("dd".equals(null));
//    System.out.println(MoreThanHalfNum_Solution(num));
  }

    /**
     * 编写一个高效的算法来搜索 m x n 矩阵 matrix 中的一个目标值 target 。该矩阵具有以下特性：
     * 每行的元素从左到右升序排列。
     * 每列的元素从上到下升序排列。
     * @param matrix
     * @param target
     * @return
     */
  public boolean searchMatrix(int[][] matrix, int target) {

    int m = matrix.length, n = matrix[0].length;
    int x = 0, y = n - 1;
    while (x < m && y >= 0) {
      if (matrix[x][y] == target) {
        return true;
      }
      if (matrix[x][y] > target) {
        --y;
      } else {
        ++x;
      }
    }
    return false;
//    if(matrix == null || matrix.length == 0) return false;
//
//    for(int i = 0; i < matrix.length; i++){
//      int[] arr = matrix[i];
//      int left = 0, rigth = arr.length;
//      while (left < rigth){
//        int mid = left + (rigth - left)/2;
//        if(arr[mid] > target){
//          rigth = mid;
//        }else if(arr[mid] < target){
//          left = mid + 1;
//        }else {
//          return true;
//        }
//      }
//    }
//
//    return false;
  }

  /**
   * 给定整数数组 nums 和整数 k，请返回数组中第 k 个最大的元素。
   * 请注意，你需要找的是数组排序后的第 k 个最大的元素，而不是第 k 个不同的元素。
   * 你必须设计并实现时间复杂度为 O(n) 的算法解决此问题。

   * @param nums
   * @param k
   * @return
   */
  public int findKthLargest(int[] nums, int k) {
    return 0;
  }

  /**
   * 查找指定值是否存在，左上角最小，右下角最大
   * @param target
   * @param array
   * @return
   */
  public boolean Find(int target, int [][] array) {
    //m为行，n为列
    int maxM = array.length;//最大行数
    if(maxM == 0) return false;
    int n = array[0].length;//最大列数
    int m = 0; n = n -1;//数组越界
    while (m < maxM && n >= 0){
      if(target > array[m][n]){
        ++m;
      }else if(target < array[m][n]){
        --n;
      }else {
        return true;
      }
    }
    return false;
  }

  /**
   * 给定一个数组，找出其中最小的K个数。例如数组元素是4,5,1,6,2,7,3,8这8个数字，
   * 则最小的4个数字是1,2,3,4。如果K>数组的长度，那么返回一个空的数组
   *
   * topk 小问题
   *
   * @param input
   * @param k
   * @return
   */
  public List<Integer> GetLeastNumbers_Solution(int [] input, int k) {

    int[] dun = Arrays.copyOfRange(input, 0, k);

    //构建堆
    buildHead(dun);
    for(int i = k; i<input.length; i++){
//      if(dun[0] > ){}
    }



    //主要在于排序，使用快排
    return Collections.emptyList();
  }

  public void reBuildHead(int[] num, int target){

  }

  public void buildHead(int [] num){
    //堆从低向上处理
    for(int i = num.length/2; i <=0; i--){
      int left = 2 * i;
      if(left > num.length) continue;

      int right = left + 1;
      int _swap = left;
      if(right <= num.length){
        if(num[left] > num[right]){
          _swap = right;
        }
      }
      if(num[i] > num[right]){
        swap(i, left, num);
      }
    }
  }

  /**
   * 给定一个数组arr，返回arr的最长无的重复子串的长度(无重复指的是所有数字都不相同)。
   * 输入 [2,3,4,5]，输出 4
   * @param arr int整型一维数组 the array
   * @return int整型
   */
  public static int maxLength (int[] arr) {
    int left = 0;
    int max = 0;
    for(int i = 1; i < arr.length; i++){
      //二次比较，看看当前arr[i]是否在left-i中
      boolean isCom = false;
      for(int j = left; j < i; j++){
        if(arr[j] == arr[i]){
          left = j + 1;
          isCom = true;
        }
      }
      int cMax = isCom ? i - left : i - left + 1;
      max = cMax > max? cMax: max;
    }
    return max;
  }


  public static int MoreThanHalfNum_Solution(int [] array) {
    int concurr = 0;
    int count = 0;

    for(int i = 0; i< array.length; i++){
      int value = array[i];
      if(count == 0){
        ++count;
        concurr = value;
      }else{
        if(concurr == array[i]){
          ++count;
        }else{
          --count;
        }
      }

    }
    count = 0;
    for(int i = 0; i< array.length; i++){
      if(concurr == array[i]){
        ++count;
      }
    }
    int mod = array.length/2;
    return  count > mod ? concurr : 0 ;

  }

  /**
   *   给定一个m x n大小的矩阵（m行，n列），按螺旋的顺序返回矩阵中的所有元素。
   */
  public ArrayList<Integer> spiralOrder(int[][] matrix) {
    ArrayList<Integer> list = new ArrayList<Integer>();

    if(matrix == null || matrix.length == 0) return list;

    boolean isForward = true;

    int rStart = 0, cStart = 0;

    //行遍历
    for(int i = 0; i < matrix.length; i++){
      //一行数据
      for(int j = 0; j < matrix[0].length; j++){
        if(isForward){
          list.add(matrix[i][j]);
        }
      }



      if(isForward){

      }

    }

 return null;

  }

  /**
   * 给定一个数组和滑动窗口的大小，找出所有滑动窗口里数值的最大值。
   * 例如，如果输入数组{2,3,4,2,6,2,5,1}及滑动窗口的大小3，那么一共存在6个滑动窗口，
   * 他们的最大值分别为{4,4,6,6,6,5}； 针对数组{2,3,4,2,6,2,5,1}的滑动窗口有以下6个：
   * {[2,3,4],2,6,2,5,1}，
   * {2,[3,4,2],6,2,5,1}，
   * {2,3,[4,2,6],2,5,1}，
   * {2,3,4,[2,6,2],5,1}，
   * {2,3,4,2,[6,2,5],1}，
   * {2,3,4,2,6,[2,5,1]}。
   * 窗口大于数组长度的时候，返回空
   * @param num
   * @param size
   * @return
   */
  public static ArrayList<Integer> maxInWindows(int [] num, int size) {
    ArrayList<Integer> list = new ArrayList<Integer>();

    if(num == null || num.length == 0 || size == 0 ||  num.length < size) return list;

    for(int i = size - 1; i < num.length; i++){

      int max = num[i];
      for(int j = i - size +1; j<= i; j++){
           max = Math.max(max, num[j]);
      }
      list.add(max);
    }

    return list;
  }


  public void swap(int i, int j, int[] num){
    int c = num[i];
    int num_j = num[j];
    num[j] = c;
    num[i] = num_j;
  }

}
