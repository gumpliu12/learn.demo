package com.gump.algorithm.strring;

import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

/**
 * @Description: 字符串操作
 * @Author gumpLiu
 * @Date 2021-04-26
 * @Version V1.0
 **/
public class StringOperation {

    public static void main(String[] args) {
//"100[leetcode] "3[z]2[2[y]pq4[2[jk]e1[f]]]ef" yy pq jkjk
        // 2 4
//        decodeString("2[2[y]pq4[2[jk]]e]"); //aaaaaabbcbbc
//        System.out.println(Math.pow(2, 31) -1);
//        System.out.println(Long.valueOf(1 << 31) -1);
        String url1 = "http://127.0.0.1:8888/portal/test/js/cont.ctl";

        String url = "/portal/test/js/cont.ctl";
        String[] strs= url.split("/");
        for(String str : strs){
            System.out.println(str);
        }
        String[] strss = url1.split(strs[1]);
        for(String str : strss){
            System.out.println(str);
        }

    }

    /**
     * 给定一个经过编码的字符串，返回它解码后的字符串。
     * 编码规则为: k[encoded_string]，表示其中方括号内部的 encoded_string 正好重复 k 次。注意 k 保证为正整数。
     * 你可以认为输入字符串总是有效的；输入字符串中没有额外的空格，且输入的方括号总是符合格式要求的。
     * 此外，你可以认为原始数据不包含数字，所有的数字只表示重复的次数 k ，例如不会出现像 3a 或 2[4] 的输入。
     * @param s  hjv2[a2[bc]3[b]]  bc3[b]2[c]  3[a2[c]] accaccacc
     *           3[z]2[2[y]pq4[2[jk]e1[f]]]ef "2[2[b]]"  "2[2[y]pq4[2[jk]]e]"
     *           "yypqjkjk yypqjkjk yypqjkjk yypqjkjke    yypqjkjkyypqjkjkyypqjkjkyypqjkjke"
     *           "yypqjkjkjkjkjkjkjkjkeyypqjkjkjkjkjkjkjkjke"
     *            yypqjkjkjkjkjkjkjkjkeyypqjkjkjkjkjkjkjkjke
     * @return
     */
    public static String decodeString(String s) {
        Stack<String> strstack = new Stack<>();
        Stack<Integer> numstack = new Stack<>();
        StringBuffer result = new StringBuffer();
        for(int i = 0; i < s.length();){
            if (Character.isDigit(s.charAt(i))){
                i = pushnumber(i, s, numstack);
            }else if(s.charAt(i) == '['){
                i = pushstr(i, s, strstack);
            }else if(s.charAt(i) == ']'){
                StringBuffer str = new StringBuffer();
                while (i < s.length() && s.charAt(i) == ']'){
                    StringBuffer value = new StringBuffer(strstack.isEmpty() ? str.toString() : strstack.pop() + str.toString());
                    StringBuffer newvalue = new StringBuffer();
                    int len = numstack.pop();
                    for(int j = 0; j < len; j++){
                        newvalue.append(value);
                    }
                    str = newvalue;
                    if(numstack.size() > strstack.size()){
                        strstack.push("");
                    }
                    i++;
                }
                if(numstack.isEmpty()) result.append(str.toString());
                else{
                    if(strstack.size() != numstack.size()){
                        strstack.push(str.toString());
                    }else{
                        strstack.push(strstack.isEmpty()? str.toString() : strstack.pop() + str.toString());
                    }
                }
            }else{
                StringBuffer strvalue = new StringBuffer();
                strvalue.append(s.charAt(i));
                i++;
                while (i < s.length() && !Character.isDigit(s.charAt(i)) && s.charAt(i) != '[' && s.charAt(i) !=']'){
                    strvalue.append(s.charAt(i++));
                }
                if(strstack.size() == numstack.size()){

                    strstack.push(strstack.isEmpty() ? strvalue.toString() : strstack.pop() + strvalue.toString()  );
                }

                if(numstack.isEmpty()) result.append(strstack.pop());
            }
        }
        return result.toString();
    }
    public static int pushnumber(int i, String s, Stack<Integer> stack){
        StringBuffer number = new StringBuffer();
        number.append(s.charAt(i++));
        while (Character.isDigit(s.charAt(i))){
            number.append(s.charAt(i++));
        }
        stack.push(Integer.parseInt(number.toString()));
        return i;
    }
    public static int pushstr(int i, String s, Stack<String> stack){
        if(Character.isDigit(s.charAt(i+1))){
            return ++i;
        }

        StringBuffer number = new StringBuffer();
        number.append(s.charAt(++i));
        i++;
        while (!Character.isDigit(s.charAt(i)) && s.charAt(i) != '[' && s.charAt(i) !=']'){
            number.append(s.charAt(i++));
        }
        stack.push(number.toString());
        return i;
    }

    int ptr;
    public String decodeStringLeetcode(String s) {
        LinkedList<String> stk = new LinkedList<String>();
        ptr = 0;
        while (ptr < s.length()) {
            char cur = s.charAt(ptr);
            if (Character.isDigit(cur)) {
                // 获取一个数字并进栈
                String digits = getDigits(s);
                stk.addLast(digits);
            } else if (Character.isLetter(cur) || cur == '[') {
                // 获取一个字母并进栈
                stk.addLast(String.valueOf(s.charAt(ptr++)));
            } else {
                ++ptr;
                LinkedList<String> sub = new LinkedList<String>();
                while (!"[".equals(stk.peekLast())) {
                    sub.addLast(stk.removeLast());
                }
                Collections.reverse(sub);
                // 左括号出栈
                stk.removeLast();
                // 此时栈顶为当前 sub 对应的字符串应该出现的次数
                int repTime = Integer.parseInt(stk.removeLast());
                StringBuffer t = new StringBuffer();
                String o = getString(sub);
                // 构造字符串
                while (repTime-- > 0) {
                    t.append(o);
                }
                // 将构造好的字符串入栈
                stk.addLast(t.toString());
            }
        }

        return getString(stk);
    }

    public String getDigits(String s) {
        StringBuffer ret = new StringBuffer();
        while (Character.isDigit(s.charAt(ptr))) {
            ret.append(s.charAt(ptr++));
        }
        return ret.toString();
    }

    public String getString(LinkedList<String> v) {
        StringBuffer ret = new StringBuffer();
        for (String s : v) {
            ret.append(s);
        }
        return ret.toString();
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////
    String src;
//    int ptr;

    public String decodeStringDG(String s) {
        src = s;
        ptr = 0;
        return getString();
    }

    public String getString() {
        if (ptr == src.length() || src.charAt(ptr) == ']') {
            // String -> EPS
            return "";
        }

        char cur = src.charAt(ptr);
        int repTime = 1;
        String ret = "";

        if (Character.isDigit(cur)) {
            // String -> Digits [ String ] String
            // 解析 Digits
            repTime = getDigits();
            // 过滤左括号
            ++ptr;
            // 解析 String
            String str = getString();
            // 过滤右括号
            ++ptr;
            // 构造字符串
            while (repTime-- > 0) {
                ret += str;
            }
        } else if (Character.isLetter(cur)) {
            // String -> Char String
            // 解析 Char
            ret = String.valueOf(src.charAt(ptr++));
        }

        return ret + getString();
    }

    public int getDigits() {
        int ret = 0;
        while (ptr < src.length() && Character.isDigit(src.charAt(ptr))) {
            ret = ret * 10 + src.charAt(ptr++) - '0';
        }
        return ret;
    }


    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     * 计算两个数之和
     * @param s string字符串 表示第一个整数
     * @param t string字符串 表示第二个整数
     * @return string字符串
     */
    public static String solve (String s, String t) {
        // write code here
        if(s == null || s.length() == 0) return t;
        if(t == null || t.length() == 0) return s;

        char[] tC = t.toCharArray();
        char[] sC = s.toCharArray();

        char[] sum = new char[tC.length > sC.length ? tC.length : sC.length];

        char jw = '\0';
        int tCurrLen = tC.length, sCurrLen = sC.length;

        for(int i =  sum.length - 1; i >= 0; i--){
            --tCurrLen;--sCurrLen;

            int currVaule = sum[i] == '\0' ? 0 : Character.getNumericValue(sum[i]);
            int iSum = 0;
            if(tCurrLen >= 0 && sCurrLen >= 0){
                iSum = Character.getNumericValue(tC[tCurrLen]) +  Character.getNumericValue(sC[sCurrLen]) + currVaule;
            }else{
                if(tCurrLen >= 0){
                    iSum = Character.getNumericValue(tC[tCurrLen]) + currVaule;
                }
                if(sCurrLen >= 0){
                    iSum = Character.getNumericValue(sC[sCurrLen]) + currVaule;
                }
            }
            int value = iSum > 9 ? (iSum - 10) : iSum;
            sum[i] = value == 0 ? ',' :  (char)(value + '0');
            if(iSum > 9){
                if(i > 0){
                    sum[i-1] = '1';
                }else{
                    jw = '1';
                }
            }

        }

        String str =  String.valueOf(sum).replaceAll(",","0");
        if(jw != '\0') return jw + str;

        return str;
    }



    /**
     * 反转字符串
     * @param str string字符串
     * @return string字符串
     */
    public static String solve (String str) {
        // write code here
        if(str == null || str.equals("")) return "";

        char [] chars = str.toCharArray();

        for(int i = 0; i< chars.length/2; i++){
            swap(i, chars.length - 1 - i, chars);
        }
        return new String(chars);
    }


    /**
     * 给出一个仅包含字符'(',')','{','}','['和']',的字符串，判断给出的字符串是否是合法的括号序列
     * 括号必须以正确的顺序关闭，"()"和"()[]{}"都是合法的括号序列，但"(]"和"([)]"不合法。
     * @param s string字符串
     * @return bool布尔型
     */
    public static boolean isValid (String s) {
        char[] chars = s.toCharArray();
        Stack stack = new Stack();
        for(char c : chars){
            switch (c){
                case '(' :
                case '{' :
                case '[' :
                    stack.push(c);
                    break;
                case ')' :
                     if(stack.empty()) return false;
                     if(!stack.pop().equals('(')){
                         return false;
                     }
                    break;
                case '}' :
                    if(stack.empty()) return false;
                    if(!stack.pop().equals('{')){
                        return false;
                    }
                    break;
                case ']' :
                    if(stack.empty()) return false;
                    if(!stack.pop().equals('[')){
                        return false;
                    }
                    break;
                default:
                    return false;
            }

        }
        return stack.empty();
    }


    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 请实现有重复数字的升序数组的二分查找
     * 给定一个 元素有序的（升序）整型数组 nums 和一个目标值 target  ，
     * 写一个函数搜索 nums 中的 target，如果目标值存在返回下标，否则返回 -1
     *
     * 如果目标值存在返回下标，否则返回 -1
     * @param nums int整型一维数组
     * @param target int整型
     * @return int整型
     */
    public static int search (int[] nums, int target) {
        // write code here
        if(nums == null || nums.length == 0) return -1;


        int start = 0;
        int end = nums.length;
        int index = -1;
        while (start <= end){
            int mod = (end + start)/2;
            if(nums[mod] == target){
                index = mod;
                break;
            }else if(nums[mod] > target){
                end = mod - 1;
            }else if(nums[mod] < target){
                start = mod + 1;
            }
        }

        if(index == -1) return -1;

        for(int i = index - 1; i>=0; i--){
            if(nums[i] == target){
                --index;
            }else {
                return i + 1;
            }

        }

        return index;
    }

//    /**
//     * 统计回文个数
//     * @param s
//     * @return
//     */
//    public int countSubstrings(String s) {
//
//
//    }


    /**
     * 对于一个字符串，请设计一个高效算法，计算其中最长回文子串的长度。
     * 给定字符串A以及它的长度n，请返回最长回文子串的长度。
     *
     *  如：输入 "abc1234321ab",12，返回7
     * @param A
     * @param n
     * @return
     */
    public static int getLongestPalindrome(String A, int n) {
        if( n< 2) return n;
        char[] chars = A.toCharArray();
        int max = 0;
        for(int i = 1; i< chars.length; i++){
            if(chars[i] == chars[i-1]){
                int low = i - 2;
                int end = i + 1;
               int maxVaue = 2;
               while (low>=0 && end < chars.length){
                   if(chars[low] == chars[end]){
                       --low;
                       ++end;
                       maxVaue+=2;
                   }else{
                       break;
                   }
               }
               max = Math.max(maxVaue, max);
            }

            if(i-1 >=0 && i+1<chars.length  && chars[i-1] == chars[i+1]){
                int low = i - 2;
                int end = i + 2;
                int maxVaue = 3;
                while (low>=0 && end < chars.length){
                    if(chars[low] == chars[end]){
                        --low;
                        ++end;
                        maxVaue+=2;
                    }else{
                        break;
                    }
                }
                max = Math.max(maxVaue, max);

            }

        }

        return max;
    }

    /**
     * 给定两个字符串str1和str2,输出两个字符串的最长公共子串
     * 题目保证str1和str2的最长公共子串存在且唯一。
     * 输入：
     *      "1AB2345CD","12345EF"
     * 输出：
     *      "2345"
     *
     * longest common substring
     * @param str1 string字符串 the string
     * @param str2 string字符串 the string
     * @return string字符串
     */
    public String LCS (String str1, String str2) {
        // write code here
        return "";
    }


    /**
     * 将给出的32位整数x翻转。
     * 例1:x=123，返回321
     * 例2:x=-123，返回-321
     *
     * 你有注意到翻转后的整数可能溢出吗？因为给出的是32位整数，则其数值范围为[−2^31, 2^31 − 1]
     * 。翻转可能会导致溢出，如果反转后的结果会溢出就返回 0。
     * @param x int整型
     * @return int整型
     */
    public static int reverse (int x) {
        // write code here
        if( x > -10 && x < 10) return x;

        boolean fs = false;
        if(x < 0){
            x = x * -1;
            fs = true;
        }

        int length =  Integer.valueOf(x).toString().length();
        int value = 0;
        for(int i = 1; i<= length; i++){
            int num = (int) (x % Math.pow(10, i) /  Math.pow(10, i-1));
            if((value > Math.pow(2, 31) -1 ) || (fs && -1 * value < Math.pow(-2, 31))) return 0;
            value +=  num * Math.pow(10, length - i);
        }

        return fs ? -1 * value : value;
    }


    public static void swap(int i, int j, char[] num){
        char c = num[i];
        char num_j = num[j];
        num[j] = c;
        num[i] = num_j;
    }

}
