package com.gump.algorithm.stack;

import java.util.Stack;

/**
 * @Description: java 栈操作
 * @Author gumpLiu
 * @Date 2021-04-23
 * @Version V1.0
 **/
public class StackSolution {

    public static void main(String[] args) {

        int[] temperatures = {73,74,75,71,69,72,76,73};
        dailyTemperatures(temperatures);

    }

    /**
     * 请根据每日 气温 列表，重新生成一个列表。对应位置的输出为：要想观测到更高的气温，至少需要等待的天数。如果气温在这之后都不会升高，请在该位置用 0 来代替。
     * 例如，给定一个列表 temperatures = [73, 74, 75, 71, 69, 72, 76, 73]，你的输出应该是 [1, 1, 4, 2, 1, 1, 0, 0]。
     * 提示：气温 列表长度的范围是 [1, 30000]。每个气温的值的均为华氏度，都是在 [30, 100] 范围内的整数。
     * @param temperatures
     * @return
     */
    public static int[] dailyTemperatures(int[] temperatures) {
        if(temperatures == null || temperatures.length == 0) return null;

        int[] result = new int[temperatures.length];

        Stack<Integer> stack = new Stack<Integer>();
        stack.push(0);
        for(int i = 1; i < temperatures.length; i++){
            while (!stack.isEmpty() && temperatures[stack.peek()] < temperatures[i]){
                result[stack.peek()] = i - stack.peek();
                stack.pop();
            }
            stack.push(i);
        }
        while (!stack.isEmpty()){
            result[stack.pop()] = 0;
        }
        return result;
    }

    /**
     * 给定一个循环数组 nums （ nums[nums.length - 1] 的下一个元素是 nums[0] ），
     * 返回 nums 中每个元素的 下一个更大元素 。
     * 数字 x 的 下一个更大的元素 是按数组遍历顺序，这个数字之后的第一个比它更大的数，
     * 这意味着你应该循环地搜索它的下一个更大的数。如果不存在，则输出 -1 。
     * @param nums
     * @return
     */
    public int[] nextGreaterElements(int[] nums) {
        if(nums == null || nums.length == 0) return null;
        int[] result = new int[nums.length];
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(0);
        for(int i = 1; i < nums.length; i++){
            while (!stack.isEmpty() && nums[stack.peek()] < nums[i]){
                result[stack.pop()] = nums[i];
            }
            stack.push(i);
        }
        if(!stack.isEmpty()){
            for(int i = 0; i < nums.length; i++){
                if(stack.isEmpty()) break;
                while (nums[stack.peek()] < nums[i]){
                    result[stack.pop()] = nums[i];
                }
            }
        }
        while (!stack.isEmpty()){
            result[stack.pop()] = -1;
        }
        return  result;
    }
}
