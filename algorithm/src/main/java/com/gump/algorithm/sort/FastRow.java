package com.gump.algorithm.sort;

/**
 *   Exception
 * 
 * @author LSP
 *
 */
public class FastRow {
	
	/**
	 * 测试
	 * @param args
	 */
	public static void main(String[] args) {
//		int[] num = {1332802,1177178,1514891,871248,753214,123866,1615405,328656,1540395,968891,1884022,252932,1034406,1455178,821713,486232,860175,1896237,852300,566715,1285209,1845742,883142,259266,520911,1844960,218188,1528217,332380,261485,1111670,16920,1249664,1199799,1959818,1546744,1904944,51047,1176397,190970,48715,349690,673887,1648782,1010556,1165786,937247,986578,798663};
//		System.out.println("1.0.0-SNAPSHOT".toLowerCase().replaceAll("[^0-9]", ""));
//		System.out.println(maximumGap(num));

		System.out.println(10%9);
	}

	/**
	 * 有一个整数数组，请你根据快速排序的思路，找出数组中第K大的数。
	 *
	 * 给定一个整数数组a,同时给定它的大小n和要找的K(K在1到n之间)，请返回第K大的数，保证答案存在。
	 *
	 * @param a
	 * @param n
	 * @param K
	 * @return
	 */
	public int findKth(int[] a, int n, int K) {
		// write code here

		return a[K - 1];
	}

	/**
	 * 双轴排序算法
	 */
	public static void quickDubSort(int[] num, int left, int right){

		if(left >= right) return;

		int leftRegionKey = num[left];
		int rightRegionKey = num[right];

		if(leftRegionKey > rightRegionKey){
			swap(num, left, right);
		}


		int l = left++, r = right--;

		while (l < r){
			//
//			while ()



		}








	}


	/**
	 * 快速排序算法
	 */
	public static void quickKSort(int[] num, int left, int right, int k){

		if(left >= right) return;

		int i = left, j = right;
		int index = num[left];
		while (left < right){
			//从right 开始
			while (left < right && num[right] > index){
				right--;
			}
			//num[right] < index
			if(left < right){
				num[left++] = num[right];
			}

			while (left < right && num[left] < index){
				left++;
			}

			if(left < right){
				num[right--] = num[left];
			}
		}
		num[left] = index;
		if(left > k){
			quickKSort(num,i,left-1,k);
		}else{
			quickKSort(num,left+1,j, k);
		}
	}


	public static void swap(int[] num, int i, int j){
		int swapNum = num[i];
		num[i] = num[j];
		num[j] = swapNum;
	}
	
	public static int maximumGap(int[] nums) {
		
		int maxiMum = 0;
		
		for(int i = 0; i < nums.length; i ++) {
			if(i == nums.length - 1) {
				break;
			}
			int differ = abs(nums[i] - nums[i+1]);
			if(differ > maxiMum ) {
				maxiMum = differ;
			}
		}
		
		
	     return maxiMum;
	}

	
	public static int abs(int a) {
        return (a < 0) ? -a : a;
	}

	
	/**
	 * 快速排序
	 * @param num	排序的数组
	 * @param left	数组的前针
	 * @param right 数组后针
	 */
	private static void QuickSort(int[] num, int left, int right) {
		//如果left等于right，即数组只有一个元素，直接返回
		if(left>=right) {
			return;
		}
		//设置最左边的元素为基准值
		int key=num[left];
		//数组中比key小的放在左边，比key大的放在右边，key值下标为i
		int i=left;
		int j=right;
		while(i<j){
			//j向左移，直到遇到比key小的值
			while(num[j]>=key && i<j){
				j--;
			}
			//i向右移，直到遇到比key大的值
			while(num[i]<=key && i<j){
				i++;
			}
			//i和j指向的元素交换
			if(i<j){
				int temp=num[i];
				num[i]=num[j];
				num[j]=temp;
			}
		}
		num[left]=num[i];
		num[i]=key;
		QuickSort(num,left,i-1);
		QuickSort(num,i+1,right);
	}
	
	/**
	 * 将一个int类型数组转化为字符串
	 * @param arr
	 * @param flag
	 * @return
	 */
	private static String arrayToString(int[] arr,String flag) {
		String str = "数组为("+flag+")：";
		for(int a : arr) {
			str += a + "\t";
		}
		return str;
	}
	
}
