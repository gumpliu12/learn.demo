package com.gump.algorithm.tree;

import java.util.*;

/**
 * 树相关算法
 */
public class Tree {


  /**
   * 分别按照二叉树先序，中序和后序打印所有的节点。
   *
   * @param root TreeNode类 the root of binary tree
   * @return int整型二维数组
   */
  static int countNum = 0;
  static int preNum = 0;
  static int inorderNum = 0;
  static int afterNum = 0;

  public static void main(String[] args) {
//        TreeNode node = new TreeNode(1);
//        TreeNode node2 = new TreeNode(2);
////        TreeNode node3 = new TreeNode(3);
////        TreeNode node4 = new TreeNode(4);
////        TreeNode node5 = new TreeNode(5);
////        TreeNode node6 = new TreeNode(6);
////        TreeNode node7 = new TreeNode(7);
////        TreeNode node8 = new TreeNode(8);
////        TreeNode node9 = new TreeNode(9);
////        node.left = node2;
////        node.right = node3;
////        node2.left = node4;
////        node2.right = node5;
////        node3.left = node6;
////        node3.right = node7;
////        node4.left = node8;
////        node4.right = node9;
    Interval interval1 = new Interval(0, 1);
    Interval interval2 = new Interval(1, 5);
    Interval interval3 = new Interval(1, 2);
    Interval interval4 = new Interval(2, 3);
    Interval interval5 = new Interval(2, 4);

    Interval[] nums = {interval1,interval2,interval3,interval4,interval5};

    int[] values = {-1,0,1,2};

//      sortedArrayToBST(values);
      //5,[3,2,1,4,5],[1,5,4,2,3]
//      int [] pre = {3,2,1,4,5};
//      int [] suf = {1,5,4,2,3};
//
//      inorderTraversal(5, pre, suf);
//      int [] u = {1,1,3,1,2,3,2,2,1,5,1,1,3,13,1,6,7,17,7,9,5,14,10,9,21,3,19,7,24,25,28,3,25,6,21,25,13,37,27,2,10,1,15,37,37,1,19,12,4,49,18,41,43,34,12,33,1,45,20,7,61,38,22,62,16,30,18,7,4,66,47,57,73,73,46,71,56,19,47,57,29,81,1,63,74,52,61};
//      int [] v = {2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88};
//      int [] x = {2623,773,9,6337,9855,1321,1485,7407,5145,7603,1671,4201,4965,6685,1545,7388,6827,457,4685,4987,7969,3421,3793,8251,5465,2014,8363,6237,6389,1469,9957,47,5565,7982,7361,4981,8407,9660,3851,5209,3851,3169,6790,3156,3410,5249,761,4243,1230,541,6685,3571,7452,506,3264,5021,6557,3963,1761,9959,181,6939,8621,6417,2995,5155,7956,4379,6751,3173,6739,9629,4347,8716,7376,4197,3881,9841,4577,2721,9011,6191,5605,4097,2889,1737,7217,505};
//      connectePiece(88,5,66752, u,v,x);

//    5,[(2,5),(5,3),(5,4),(5,1)],2,[(1, 3), (2, -1)]
    Point point1 = new Point(1,2);
    Point point2 = new Point(2,3);
    Point point3 = new Point(3,4);
    Point point4 = new Point(2,5);
    Point[] points = {point1,point2,point3,point4};

//    Point eg1 = new Point(1,3);
//
//    Point eg2 = new Point(2,-1);
//    Point[] egs = {eg1,eg2};

//    maxNodeNum(5, 2, points);
//
//      int[] a = {1,2,3,4,5};
//      tree1(a);

      int[] a = {1,2,3,4,5};

      TreeNode node = new TreeNode(1);
      TreeNode node2 = new TreeNode(2);
      TreeNode node3 = new TreeNode(3);
      TreeNode node4 = new TreeNode(5);
//      TreeNode node5 = new TreeNode(7);
//      TreeNode node6 = new TreeNode(9);
//      TreeNode node7 = new TreeNode(11);
        node.left = node2;
        node.right = node3;
        node2.right = node4;
//        node3.left = node6;
//        node3.right = node7;

//      treeSerialize(node);
      binaryTreePaths(node);
    System.out.println(Math.log(9)/Math.log(2));

  }

    public static List<String> binaryTreePaths(TreeNode root) {
        List<String> paths = new ArrayList<String>();
        treePath(root, paths, null);
        return paths;

    }

    public static void treePath(TreeNode root, List<String> paths, String path){
        if(root == null) return;

        if(root.left == null && root.right== null){
            paths.add("->" + root.val);
        }
        path = path == null ? root.val +"" : "->" + root.val;
        treePath(root.left, paths, path);
        treePath(root.right, paths, path);
    }


    public List<TreeNode> generateTrees(int n) {
        return build(1,n);
    }
    List<TreeNode> build(int low,int high) {
        List<TreeNode> list = new ArrayList();
        if(low > high) {
            //空指针也视为一种情况
            list.add(null);
            return list;
        }
        //遍历每一个结点作为根节点
        for(int i=low;i<=high;i++){
            //构建左右子树
            List<TreeNode> left = build(low,i-1);
            List<TreeNode> right = build(i+1,high);
            //遍历左右子树 再利用上根节点[i] 来构建完整的树
            for(TreeNode leftNode : left) {
                for(TreeNode rightNode : right) {
                    TreeNode root = new TreeNode(i);
                    root.left = leftNode;
                    root.right = rightNode;
                    list.add(root);
                }
            }
        }
        return list;
    }

    /**
     * 给出一棵有n个结点的标准的完全二叉树（即，若父结点编号为x，则它的两个子结点的编号分别为2x和x×2+1）
     * 定义每个结点的价值为xdepx即每个点的编号乘以每个点的深度
     * （深度即为从根结点到这一结点最短路径经过的点的个数，定义根结点1的深度为1）。
     * @param n
     * @return
     */
    public static long tree4 (long n) {
        // write code here
//        long dep = 1;
//        long sum = 0;
//        int mod = 998244353;
//        long res = 0;
//        long i = 1;
//        for(i = 1; i*2<=n; i*=2){
//            sum = (i+i*2-1)*i/2%mod;
//            res += sum*dep%mod;
//            dep++;
//        }
//        sum = (i+n)*(n-i+1)/2%mod;
//        res = (res + sum*dep)%mod;
//        return res;

        if(n == 0) return 0;
        long mid = n/2;
        long sum = 1;
        int preHight = 0;
        for(long i = 1; i <= mid; i++){
            long left = i<<1;
            if((long)Math.pow(2, preHight) <= i){
                preHight++;
            }
            int hight = preHight + 1;

            if(left > n) break;

            if(left+1 > n)
                sum = (left * hight + sum) % 998244353;
            else
                sum = ((((left<<1) + 1) * hight) + sum) % 998244353;
        }
        return sum ;
    }


    /**
     * 树 序列化
     * @param root
     * @return
     */
    public static String treeSerialize(TreeNode root) {
        //层级
        StringBuffer treeStr = new StringBuffer("");

        if(root == null) return treeStr.toString();
        //层级遍历
        Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
        queue.offer(root);
        treeStr.append(root.val);
        TreeNode nullTree = new TreeNode(-1);

        int count = 0, nextCount = 1, hight = 0, countNull = 0;
        while (!queue.isEmpty()){

            TreeNode cNode = queue.poll();
            count++;

            TreeNode left = cNode.left;
            if(left != null){
                queue.offer(left);
            }else{
                queue.offer(nullTree);
                countNull++;
            }
            treeStr.append(",");
            treeStr.append(left != nullTree && left != null ? left.val : "#");

            TreeNode right = cNode.right;
            if(right != null){
                queue.offer(right);
            }else {
                queue.offer(nullTree);
                countNull++;
            }
            treeStr.append(",");
            treeStr.append(right != nullTree && right != null ? right.val : "#");

            if(count == nextCount){

                if(countNull == Math.pow(2, hight+1)) break;

                ++hight;
                count = 0;
                countNull = 0;
                nextCount = queue.size();
            }
        }

        String value = treeStr.toString();

        return value.substring(0, value.length() - (int) Math.pow(2, hight + 1) * 2);
    }//      treeDeserialize("8,6,10,5,7,9,11");
    public static TreeNode treeDeserialize(String str) {

        if(str == null || str.length() == 0) return null;

        String[] chars = str.split(",");
        int mid = chars.length/2;
        TreeNode[] treeNodes = new TreeNode[chars.length];
        TreeNode root = new TreeNode(Integer.valueOf(chars[0]+""));
        treeNodes[0] = root;
        for(int i = 0; i < mid; i++){
            TreeNode cRoot = treeNodes[i];

            if(cRoot == null) continue;

            String left = chars[2*i + 1];
            if(!left.equals("#")){
                TreeNode leftTree = new TreeNode(Integer.valueOf(left));
                treeNodes[2*i + 1] = leftTree;
                cRoot.left = leftTree;
            }

            String right = chars[2*i + 2];
            if(!right.equals("#")){
                TreeNode rightTree = new TreeNode(Integer.valueOf(right));
                treeNodes[2*i + 2] = rightTree;
                cRoot.right = rightTree;
            }
        }

        return root;
    }



    /**
     * todo 需要重构
     *
     * 系统中有一棵n个点的完全二叉树，现给出它的DFS正序遍历序列ai (前序遍历)
     *  （即从根节点开始，先遍历左子树，后遍历右子树），请你还原这棵树，并返回加密后的答案。
     * @param a int整型一维数组 表示这棵完全二叉树的Dfs遍历序列的结点编号
     * @return long长整型
     */
    public long tree5 (int[] a) {
        // write code here
        int hight = (int) (Math.log(a.length) / Math.log(2));

        int upfullNum = (1 << hight) - 1;
        int[] flow = {0, a.length - upfullNum};

        long result = dfs(a, hight, 1, flow);
        return result;
    }

    public long dfs(int[] n, int h, int level, int[] flow) {
        if (flow[0] == n.length - 1) {
            return 0;
        }

        int top = n[flow[0]];
        long sum = 0;
        if (level < h) {//满二叉树
            flow[0]++;
            sum += top ^ n[flow[0]];//左节点
            sum += dfs(n, h, level + 1, flow);//左子树
            flow[0]++;
            sum += top ^ n[flow[0]];//右节点
            sum += dfs(n, h, level + 1, flow);//右子树
        } else if (level == h && flow[1] > 0) {
            flow[0]++;
            flow[1]--;
            sum += top ^ n[flow[0]];

            if (flow[1] > 0) {
                flow[0]++;
                flow[1]--;
                sum += top ^ n[flow[0]];
            }
        }

        return sum;
    }


    /**
     * 系统中有一棵n个点的完全二叉树，现给出它的BFS层序遍历序列ai
     *（从根节点开始，自上而下自左到右的一层一层遍历，即首先访问根，
     * 然后从左到右访问第2层上的节点，接着是第三层的节点），请你还原这棵树，并返回加密后的答案。
     *
     * @param a int整型一维数组 表示这棵完全二叉树的Bfs遍历序列的结点编号
     * @return long长整型
     */
    public static long tree1 (int[] a) {
        // write code here
       if(a == null || a.length <=1) return 0;
       int mid = a.length/2;

       int value = 0;
       for(int i = 0; i <=mid ; i++){
           int root = a[i];
           if(2*i +1 < a.length) {
               int left = a[2*i+1];
               value += root ^ left;
           }
           if(2*i + 2 < a.length) {
               int right = a[2*i + 2];
               value += root ^ right;
           }
       }

       return value;
    }


    /**
     * 给出一颗以1为根的树，树上节点的值只为1或者0，在最多经过两个值为1的节点的情况下，有多少条到达叶节点的路径？
     *
     * 一个整数，表示路径数
     * @param n 第一个参数为 n,(1≤n≤100000)
     * @param Edge Point一维数组 第二个参数为大小为 n-1n−1 的点对 (ui, vi)) 的集合
     * @param f int一维数组  第三个参数为大小为序列0/1序列f， fi为i-1结点的值。
     * @return int
     */
    int pathCount = 0;
    public int pathCount (int n, Point[] Edge, int[] f) {
        // write code here

        if(Edge.length == 0) return 1;

        List<Integer>[] greph = pathCountCreateGraph(n, Edge);
        boolean[] bools = new boolean[n+1];
        bools[1] = true;

        pathCountDFS(1, 0, greph, f, bools);
        return pathCount;
    }

    public void pathCountDFS(int cNode, int maxValue, List<Integer>[] greph, int[] f, boolean[] bools){

        maxValue += f[cNode-1];
        if(maxValue > 2) return;

        List<Integer> childs = greph[cNode];
        if(childs.size() == 1 && bools[childs.get(0)]){
            if(maxValue <=2){
                pathCount++;
            }
        }else{
            for(Integer node : childs){
                if(!bools[node]){
                    bools[node] = true;
                    pathCountDFS(node, maxValue, greph, f, bools);
                }
            }
        }
    }

    /**
     * 构建有向图
     *
     * @param n
     * @param edge
     * @return
     */
    public static List<Integer>[] pathCountCreateGraph(int n, Point[] edge){

        List<Integer>[] nodes = new ArrayList[n+1];
        for(int i = 1; i <= n; i++){
            nodes[i] = new ArrayList<Integer>();
        }

        for(Point point : edge){
            nodes[point.y].add(point.x);
            nodes[point.x].add(point.y);

        }
        return nodes;
    }

  /**
   * 在一颗有n个结点且以 1 为根节点树上，起初每个结点的初始权值为0现在有q 次操作，
   * 每次操作选择将以 ri为根节点的子树上的所有结点权值增加xi求 q 次操作后从 11 到 nn 每个结点的权值。
   * @param n
   * @param Edge
   * @param q
   * @param Query
   * @return
   */
  static long[] parentChildDepthQ = null;
  public static long[] parentChildDepth (int n, Point[] Edge, int q, Point[] Query) {
    // write code here
    parentChildDepthQ = new long[n];

    List<Integer>[] graph = parentChildDepthCreateGraph(n, Edge);

    Queue<Integer> queue = new ArrayDeque();
    boolean[] booleans = new boolean[n+1];
    for(int i = 0; i< q; i++){
      parentChildDepthQ[Query[i].x -1] += Query[i].y;
    }
    booleans[1] = true;

    queue.offer(1);

    while (!queue.isEmpty()){
      int cNode = queue.poll();
      for(Integer node : graph[cNode]){
        if(!booleans[node]){
          booleans[node] = true;
          parentChildDepthQ[node-1] += parentChildDepthQ[cNode-1];
          queue.offer(node);
        }
      }
    }

    return parentChildDepthQ;
  }

  /**
   * 构建有向图
   *
   * @param n
   * @param edge
   * @return
   */
  public static List<Integer>[] parentChildDepthCreateGraph(int n, Point[] edge){

    List<Integer>[] nodes = new ArrayList[n+1];
    for(int i = 1; i <= n; i++){
      nodes[i] = new ArrayList<Integer>();
    }

    for(Point point : edge){
      nodes[point.y].add(point.x);
      nodes[point.x].add(point.y);

    }
    return nodes;
  }



    /**
     * 有n个房间，房间之间有通道相连，一共有n-1个通道，
     * 每两个房间之间都可以通过通道互相到达。第i个房间有xi个金币。
     * 现在牛牛想通过封闭一些通道来把n个房间划分成k个互相不连通的区域，
     * 他希望这k个区域内部的金币数目和都大于等于m，你需要告诉他这是否可行。
     * @param n  第一个参数n代表房间数量
     * @param k  第二个参数k代表要划分成k块区域。
     * @param m  第三个参数m代表每块区域的金币数要大于等于m
     * @param u  第四、五个参数vector u,v代表通道，uiu与vi通过通道相连
     * @param v
     * @param x  第六个参数vector x代表每个房间的金币数
     * @return
     */
    static int connectePieceK = 0;
    static boolean[] ks = null;
    public static  boolean connectePiece (int n, int k, int m, int[] u, int[] v, int[] x) {

        connectePieceK = k;
        ks = new boolean[k];
        List<Integer>[] graph = connectePieceCreateGraph(n, u, v);

        connectePieceDFS(1, -1, m, x, graph);

        for(boolean bol : ks){
            if(!bol) return bol;
        }

        return true;
    }

    /**
     * 深度优先搜索
     * @param cNode
     * @param pNode
     * @param nodes
     */
    public static  int connectePieceDFS(int cNode, int pNode,int m, int[] x, List<Integer>[] nodes){

        List<Integer> cNodes = nodes[cNode];

        int mNum = x[cNode-1];
        for(int node : cNodes){
            if(node == pNode) continue;

            mNum += connectePieceDFS(node, cNode, m, x, nodes);
            if(mNum >= m && connectePieceK > 0){
                mNum = 0;
                ks[--connectePieceK] = true;
                return 0;
            }
        }
        return mNum ;
    }

    /**
     * 构建图
     * @param u
     * @param v
     * @return
     */
    public static  List<Integer>[] connectePieceCreateGraph(int n, int[] u, int[] v){

        List<Integer>[] nodes = new ArrayList[n+1];
        for(int i = 1; i<= n ; i++){
            nodes[i] = new ArrayList<Integer>();
        }

        for(int i = 0; i< u.length; i++){
            nodes[u[i]].add(v[i]);
            nodes[v[i]].add(u[i]);
        }
        return nodes;
    }

    /**
     * 寻找牛妹
     *
     * 有n个房间，房间之间有通道相连，一共有n-1个通道，每两个房间之间都可以通过通道互相到达。
     *
     * 牛牛一开始在1号房间。牛妹在某个另外的房间。牛牛想去寻找牛妹，但是他没有地图，所以只能在房间之间乱走。
     * 牛牛每走过一条通道需要花费1金币，每条通道最多只能走两次。
     * 牛牛有一个乱走规则：如果当前牛牛有 没走过的通道 可以走，牛牛就不会去走 走过一次的通道
     * 这个规则可以保证只要钱足够就一定可以找到牛妹。
     *
     * 有m个查询，每次询问如果牛妹在x_ix
     * i
     * ​
     *  号房间，请问牛牛身上至少要带多少金币才可以保证任何情况下都可以找到牛妹。
     *
     * 第一个参数n代表房间数量
     * 第二个参数m代表查询数量
     * 第三、四个参数vector u,v代表通道，ui与vi通过通道相连
     * 第五个参数vector x代表m次查询中牛妹的位置
     * @param n
     * @param m
     * @param u
     * @param v
     * @param x
     * @return
     */
    static int[] depths = null;
    static int[] childs = null;
    public static int[] findOXSister (int n, int m, int[] u, int[] v, int[] x) {
          // write code here
        int[] maxs = new int[x.length];
        List<Integer>[] nodes = findOXSisterCreateGraph(n, u, v);
        depths = new int[n];
        childs = new int[n];

        findOXSisterDFS(1, -1, 0, nodes);

        for(int i = 0; i < x.length; i++){
            maxs[i] = 2*(n-1) - depths[x[i]-1] - 2*childs[x[i]-1];
        }

        return maxs;
    }

    /**
     * 矩阵链表 构建图形结构
     * @param n
     * @param u
     * @param v
     * @return
     */
    public static List<Integer>[] findOXSisterCreateGraph(int n, int[] u, int[] v){
        List<Integer>[] nodes = new ArrayList[n+1];
        //构建图
        for(int i = 1; i< n+1; i++){
            nodes[i] = new ArrayList<Integer>();
        }

        for(int i = 0; i < u.length; i++){
            nodes[u[i]].add(v[i]);
            nodes[v[i]].add(u[i]);
        }

        return nodes;
    }

    /**
     * 深度遍历
     * @param cNode 当前节点
     * @param pNode 父节点
     * @param depth 当前深度
     * @param nodes 图
     * @return
     */
    public static int findOXSisterDFS(int cNode, int pNode, int depth, List<Integer>[] nodes){
        depths[cNode-1] = depth;
        int childCount = 0;
        List<Integer> nextList = nodes[cNode];

        for(int node : nextList){
            if(node == pNode) continue;
            childCount += findOXSisterDFS(node, cNode, depth+1, nodes);
        }
        childs[cNode-1] = childCount;
        childCount += 1;
        return childCount;
    }


    /**
     * n个节点n-1条边的无向连通图，两个点a，b，a位于1，b位于x，两点移动速度相同，求a和b移动到同一节点所需的最多节点数。
     *
     * 最多需要经过的节点数z， 转换为图的遍历
     * @param n int
     * @param x int
     * @param Edge Point一维数组
     * @return int
     */
    public static int maxNodeNum (int n, int x, Point[] Edge) {
        // write code here

        if(Edge == null || Edge.length == 0) return 0;

        List<Integer>[] graph = maxNodeNumCreateGraph(n, Edge);


        int[] rootDepth = maxNodeNumLevel(n, 1, graph);

        int[] xDepth = maxNodeNumLevel(n, x, graph);

        int max = 0;
        //todo 没有明白为啥循环比较
        for(int i = 1; i< rootDepth.length; i++){
          if(rootDepth[i] > xDepth[i]) max = Math.max(max, rootDepth[i]);
        }


        return max;

    }

    public static int[] maxNodeNumLevel(int n, int root, List<Integer>[] graph){


      boolean[] bools = new boolean[n+1];
      int[] depths = new int[n+1];
      depths[root] = 1;
      bools[root] = true;
      int count = 0, newCount = 1;
      Queue<Integer> queue = new ArrayDeque<Integer>();
      queue.offer(root);
      int depth = 2;

      while (!queue.isEmpty()){
        int cNode = queue.poll();
        ++count;
        List<Integer> childs = graph[cNode];

        for(Integer node : childs){
          if(!bools[node]){
            bools[node] = true;
            queue.offer(node);
            depths[node] = depth;

          }
        }
        if(count == newCount){
          ++depth;
          count = 0;
          newCount = queue.size();
        }

      }
      return depths;
    }

    public static List<Integer>[] maxNodeNumCreateGraph(int n, Point[] edge){

      List<Integer>[] nodes = new ArrayList[n+1];
      for(int i = 1; i <= n; i++){
        nodes[i] = new ArrayList<Integer>();
      }
      for(Point point : edge){
        nodes[point.x].add(point.y);
        nodes[point.y].add(point.x);
      }

      return nodes;

    }

    /**
     * 给定一棵有n个结点的二叉树的先序遍历与后序遍历序列，求其中序遍历序列。
     * 若某节点只有一个子结点，则此处将其看作左儿子结点
     * @param n int 二叉树节点数量
     * @param pre int一维数组 前序序列
     * @param suf int一维数组 后序序列
     * @return
     */
    public static int[] inorderTraversal (int n, int[] pre, int[] suf) {
        // write code here、
        int[] nums = new int[n];
        if(n == 0) return nums;

        inorder(0, pre.length - 1, pre, 0, suf.length - 1, suf, nums);

        return nums;
    }
    static int length = 0;
    public static void inorder (int i_pre,int i_end, int[] pre, int s_pre, int s_end, int[]suf, int[] nums) {
        if(i_pre > i_end) return;
        if(i_pre == i_end) {
            nums[length++] = pre[i_pre];
            return;
        }

        //寻找后续左节点位置
        int pos = -1;
        for(int i = s_pre; i<= s_end; i++){
            if(suf[i] == pre[i_pre + 1]){
                pos = i;
                break;
            }
        }
        //前序 第一个是根节点，左节点树，右节点树
        //后序 最后一个是根节点，
        //     左树：根据前序第二节点定位左树节点位置，左树节点向前都是为左树，
        //     右树：跟节点->左树根节点为右树，紧挨着根接点的为右根节点
        //递归左树
        inorder(i_pre+1, pos - s_pre + i_pre + 1, pre, s_pre, pos, suf, nums);
        nums[length++] = pre[i_pre];
        //递归右树
        inorder(pos - s_pre +  i_pre + 2 , i_end, pre, pos + 1, s_end -1, suf, nums);
    }


    /**
     * 寻找牛妹妹
     * @param n
     * @param m
     * @param u
     * @param v
     * @param x
     * @return
     */
    public int[] findOX (int n, int m, int[] u, int[] v, int[] x) {
        // write code here
        int[]nums = new int[n];
        if(n == 0) return nums;




        return nums;
    }


    /**
     * 扩散，构建无向图
     *
     * @param n 第一个参数{n}代表工厂数量
     * @param m 第二个参数{m}m代表生产力提升次数
     * @param u 第三个参数u和第四个参数vector<int> v各自包含n-1个元素代表管道。第i根管道连接第ui个工厂和第vi个工厂。
     * @param v
     * @param q 第五个参数q包含m个元素代表生产力提升发生的位置。
     * @return int整型一维数组
     */
    public int[] solve (int n, int m, int[] u, int[] v, int[] q) {
        // write code here
        int[]nums = new int[n];
        if(n == 0) return nums;

        Map<Integer, List<Integer>> graph = createGraph(u, v);

        for(int i = 0; i < q.length; i++){
            int start = q[i];
            nums[start - 1] = nums[start-1] + 1;
            List<Integer> list =  graph.get(q[i]);
            if(list != null && list.size() > 0){
                for(Integer end : list){
                    nums[end - 1] = nums[end - 1] + 1;
                }
            }
        }
        return nums;
    }

    /**
     * 构建无向图
     * @param u
     * @param v
     * @return
     */
    public Map<Integer, List<Integer>> createGraph(int[] u, int[] v){

        Map<Integer, List<Integer>> graph = new HashMap<Integer, List<Integer>>();

        for(int i = 0; i< u.length; i++){

            int start = u[i];
            int end = v[i];

            if(!graph.containsKey(start)){
                graph.put(start, new ArrayList<Integer>());
            }
            graph.get(start).add(end);

            if(!graph.containsKey(end)){
                graph.put(end, new ArrayList<Integer>());
            }
            graph.get(end).add(start);
        }
        return graph;
    }


    /**
     * 给出一个升序排序的数组，将其转化为平衡二叉搜索树（BST）.
     * @param num int整型一维数组
     * @return TreeNode类
     */
    public static TreeNode sortedArrayToBST (int[] num) {
        // write code here

        return createBST(num, 0, num.length - 1);

    }

    public static TreeNode createBST (int[] num, int start, int end) {
        // write code here
        if(start > end) return null;
        int mid = start + ((end - start+1) >> 1);
        TreeNode node = new TreeNode(num[mid]);
        node.left = createBST(num, start, mid - 1);
        node.right = createBST(num, mid + 1, end);
        return node;
    }




    /**
     * 给定一棵完全二叉树的头节点head，返回这棵树的节点个数。
     * 如果完全二叉树的节点数为N，请实现时间复杂度低于O(N)的解法。
     * @param head
     * @return
     */
    public static int nodeNum(TreeNode head) {
        return couintNodeNum(head);
    }
    public static int couintNodeNum(TreeNode head) {
        if(head == null) return 0;
        TreeNode l = head, r = head;
        int lh = 0; int rh = 0;
        while (l != null){
            l = l.left;
            lh++;
        }
        while (r != null){
            r = r.right;
            rh++;
        }

        if(lh == rh){
            return  (int) Math.pow(2,lh) - 1;
        }

        return 1 + couintNodeNum(head.left) + couintNodeNum(head.right);
    }

  /**
   * 给定一个二叉树的根节点root，返回它的中序遍历。
   *
   *
   * @param root TreeNode类
   * @return int整型一维数组
   */
  public int[] inorderTraversal (TreeNode root) {
    // write code here

    if(root == null) return new int[0];

    List<Integer> list = new ArrayList<Integer>();

    Stack<TreeNode> stack = new Stack<TreeNode>();
    TreeNode tail = root;

    while (!stack.isEmpty() || tail != null){
      while (tail != null){
        stack.push(tail);
        tail = tail.left;
      }

      if(!stack.isEmpty()){
        tail = stack.pop();
        list.add(tail.val);
        tail = tail.right;
      }
    }
    int[] nums = new int[list.size()];


    for (int i = 0; i< list.size(); i++){
      nums[i] = list.get(i);
    }

    return nums;

  }


  /**
   *
   * 棵二叉树原本是搜索二叉树，但是其中有两个节点调换了位置，
   * 使得这棵二叉树不再是搜索二叉树，请按升序输出这两个错误节点的值。(每个节点的值各不相同)
   *
   * @param root TreeNode类 the root
   * @return int整型一维数组
   */
  public int[] findError (TreeNode root) {
    // write code here
    int[] nums = new int[2];
    int idx = 1;
    int pre = Integer.MIN_VALUE;
    if(root == null) return nums;

    Stack<TreeNode> stackIn = new Stack<TreeNode>();
    TreeNode node = root;

    while (!stackIn.isEmpty() || node != null){
      while (node != null){
        stackIn.push(node);
        node = node.left;
      }

      if(!stackIn.isEmpty()){
        node = stackIn.pop();
        if(pre > node.val){
          if(idx == 1){
            nums[idx--] = pre;
          }else {
            nums[0] = node.val;
            return nums;
          }
        }else{
          pre = node.val;
        }
        node = node.right;
      }
    }


    return nums;

  }




  /**
   * 给定一棵二叉搜索树，请找出其中的第k小的TreeNode结点
   *
   * @param pRoot
   * @param k
   * @return
   */
  int index = 1; TreeNode kthNode = null;

  public TreeNode KthNode(TreeNode pRoot, int k) {

    if(pRoot == null || k == 0) return null;

    kthNodeIn(pRoot, k);

    return kthNode;

  }


  public void kthNodeIn(TreeNode pRoot, int k) {

    if(pRoot == null) return;

    kthNodeIn(pRoot.left, k);

    index++;

    if(k == index){
      kthNode = pRoot;
      return;
    }
    kthNodeIn(pRoot.right, k);
  }





  /**
   * 给定彼此独立的两棵二叉树，判断 t1 树是否有与 t2 树拓扑结构完全相同的子树。
   * 设 t1 树的边集为 E1，t2 树的边集为 E2，若 E2 等于 E1 ，则表示 t1 树和t2 树的拓扑结构完全相同。
   *
   * @param root1 TreeNode类
   * @param root2 TreeNode类
   * @return bool布尔型
   */
  public boolean isContains (TreeNode root1, TreeNode root2) {
    // write code here
    if(root1 == null && root2 == null) return true;
    if(root1 == null || root2 == null) return false;

    Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
    queue.offer(root1);
    while (!queue.isEmpty()){
      TreeNode currentNode = queue.peek();
      boolean isC =  compare(currentNode, root2);
      if(isC) return isC;
      if(currentNode.left != null)
        queue.offer(currentNode.left);
      if(currentNode.right != null)
        queue.offer(currentNode.right);
    }

    return false;
  }

  public boolean compare(TreeNode root1, TreeNode root2){

    if(root1 == null && root2 == null) return true;

    if(root1 == null || root2 == null) return false;

    if(root1.val != root2.val) return false;

    return compare(root1.left, root2.left) && compare(root1.right, root2.right);
  }


  public void dfsIn(TreeNode root){
    if(root == null) return;
    Stack<TreeNode> stack = new Stack();
    stack.push(root);
    TreeNode currentNode = root;
    while (!stack.isEmpty() || currentNode != null){
      while (currentNode != null){
        if(currentNode.left == null){
          currentNode = null;
        }else{
          stack.push(currentNode.left);
          currentNode = currentNode.left;
        }
      }
      currentNode = stack.pop();

      while (currentNode != null){
        if(currentNode.right == null){
          currentNode = null;
        }else {
          stack.push(currentNode.right);
          currentNode = currentNode.right;
        }
      }

    }
  }

  /**
   * 给定一棵二叉树以及这棵树上的两个节点 o1 和 o2，请找到 o1 和 o2 的最近公共祖先节点。
   * 输入：
   * [3,5,1,6,2,0,8,#,#,7,4],5,1
   * 返回：
   * 3
   *
   * @param root TreeNode类
   * @param o1   int整型
   * @param o2   int整型
   * @return int整型
   */
  public static int lowestCommonAncestor(TreeNode root, int o1, int o2) {

//        return CommonAncestor(root, o1, o2).val;
    List<TreeNode> list1 = new ArrayList<TreeNode>(), list2 = new ArrayList<TreeNode>();

    boolean r1 = routed(list1, root, o1);
    boolean r2 = routed(list2, root, o2);

    if (root == null || !r1 || !r2) return -1;

    for (TreeNode node : list1) {
      if (list2.contains(node)) return node.val;
    }

    return root.val;

  }

  /**
   * 保存路径
   */
  public static boolean routed(List<TreeNode> list, TreeNode root, int value) {

    if (root == null) return false;

    boolean rout = root.val == value || routed(list, root.left, value) || routed(list, root.right, value);

    if (rout) {
      list.add(root);
    }

    return rout;
  }

  public static TreeNode CommonAncestor(TreeNode root, int o1, int o2) {
    if (root == null || root.val == o1 || root.val == o2) { // 超过叶子节点，或者root为p、q中的一个直接返回
      return root;
    }
    TreeNode left = CommonAncestor(root.left, o1, o2); // 返回左侧的p\q节点
    TreeNode right = CommonAncestor(root.right, o1, o2); // 返回右侧的p\q节点
    if (left == null) {  // 都在右侧
      return right;
    }
    if (right == null) { // 都在左侧
      return left;
    }
    return root; // 在左右两侧
  }

  /**
   * 根据前，中序列 构建二叉树
   *
   * 前序{1,2,4,7,3,5,6,8}和中序遍历序列{4,7,2,1,5,3,8,6}
   * 1   2,4,7  4,7,2,     ,5,3,8,6
   *
   * @param in，中序列 以递归方式实现
   */
  public static TreeNode reConstructBinaryTree(int[] pre, int[] in) {
    if (pre.length <= 0 || in.length <= 0) return null;

    int rootNum = pre[0];
    TreeNode rootTree = new TreeNode(pre[0]);

    for (int i = 0; i < in.length; i++) {
      if (in[i] == rootNum) {
        rootTree.left = reConstructBinaryTree(Arrays.copyOfRange(pre, 1, i + 1),
                Arrays.copyOfRange(in, 0, i));
        rootTree.right = reConstructBinaryTree(Arrays.copyOfRange(pre, i + 1, pre.length),
                Arrays.copyOfRange(in, i + 1, in.length));
        break;
      }
    }
    return rootTree;
  }

  /**
   * 非递归
   */
  public static TreeNode reConstructBinaryTree1(int[] pre, int[] in) {
    if (pre.length <= 0 || in.length <= 0) return null;

    int rootNum = pre[0];
    TreeNode rootTree = new TreeNode(pre[0]);

    for (int i = 0; i < in.length; i++) {
      if (in[i] == rootNum) {

        break;
      }
    }
    return rootTree;
  }

  public static int[][] threeOrders(TreeNode root) {
    // write code here

    if (root == null) return null;

    countNodeNum(root);


    int[][] nums = new int[3][countNum];

    preThreOrders(root, nums);

    inorderThreOrders(root, nums);

    afterThreOrders(root, nums);

    return nums;
  }

  public static void countNodeNum(TreeNode root) {

    if (root == null) return;

    ++countNum;

    countNodeNum(root.left);
    countNodeNum(root.right);

  }

  public static void preThreOrders(TreeNode root, int[][] num) {

    if (root == null) return;

    num[0][preNum] = root.val;
    ++preNum;
    preThreOrders(root.left, num);
    preThreOrders(root.right, num);
  }

  public static void inorderThreOrders(TreeNode root, int[][] num) {

    if (root == null) return;

    inorderThreOrders(root.left, num);
    num[1][inorderNum] = root.val;
    ++inorderNum;
    inorderThreOrders(root.right, num);
  }

  public static void afterThreOrders(TreeNode root, int[][] num) {

    if (root == null) return;

    afterThreOrders(root.left, num);
    afterThreOrders(root.right, num);
    num[2][afterNum] = root.val;
    ++afterNum;
  }

  /**
   * 请根据二叉树的前序遍历，中序遍历恢复二叉树，并打印出二叉树的右视图
   *
   * 求二叉树的右视图
   *
   * @param xianxu  int整型一维数组 先序遍历
   * @param zhongxu int整型一维数组 中序遍历
   * @return int整型一维数组
   */
  public static int[] solve(int[] pre, int[] in) {
    // write code here
    if (pre.length <= 0 || in.length <= 0) return new int[0];

    List<List<Integer>> list = new ArrayList<List<Integer>>();

    List<Integer> root =new ArrayList<Integer>();
    root.add(pre[0]);
    solve1(pre, in, list, 0);
    int[] nums = new int[list.size()];

//    for (int i = 0; i < list.size(); i++) {
//      int value = list.get(i);
//      nums[i] = value;
//    }
    Arrays.sort(nums);
    return nums;
  }

  public static int solve1(int[] pre, int[] in, List<List<Integer>> list, int level) {
    // write code here
    if (pre.length <= 0 || in.length <= 0) return 0;

    int rootNum = pre[0];
    List<Integer> list1 = new ArrayList<Integer>();

    for (int i = 0; i < in.length; i++) {
      if (in[i] == rootNum) {

        int left = solve1(Arrays.copyOfRange(pre, 1, i + 1),
                Arrays.copyOfRange(in, 0, i), list, level);

        int right = solve1(Arrays.copyOfRange(pre, i + 1, pre.length),
                Arrays.copyOfRange(in, i + 1, in.length), list, level);

        if (left != 0) {
          list1.add(right);
        }
        if (right != 0) {
          list1.add(right);
        }
        break;
      }
    }

    return rootNum;
  }

  /**
   * 给定一个二叉树，返回该二叉树的之字形层序遍历，（第一层从左向右，下一层从右向左，一直这样交替）
   *
   * @param root TreeNode类
   * @return int整型ArrayList<ArrayList<>>
   */
  public ArrayList<ArrayList<Integer>> zigzagLevelOrder(TreeNode root) {
    // write code here

    ArrayList<ArrayList<Integer>> tree = new ArrayList();

    if (root == null) return tree;

    Queue queue = new ArrayDeque();
    queue.add(root);

    Stack stack = new Stack();

    boolean isLeaf = true;

    while ((!queue.isEmpty() && isLeaf) || (!stack.isEmpty() && !isLeaf)) {
      ArrayList<Integer> list = new ArrayList<Integer>();
      if (isLeaf) {
        stack = zigzagLevelQueue(queue, list);
        isLeaf = false;
      } else {
        queue = zigzagLevelStack(stack, list);
        isLeaf = true;
      }

      if (!list.isEmpty()) {
        tree.add(list);
      }
    }
    return tree;
  }

  public Stack zigzagLevelQueue(Queue queue, ArrayList<Integer> list) {

    Stack stack = new Stack();

    while (!queue.isEmpty()) {
      TreeNode node = (TreeNode) queue.poll();
      list.add(node.val);
      if (node.left != null) {
        stack.add(node.left);
      }
      if (node.right != null) {
        stack.add(node.right);
      }

    }

    return stack;
  }

  public Queue zigzagLevelStack(Stack stack, ArrayList<Integer> list) {

    Queue nodeQueue = new ArrayDeque();
    Stack stack1 = new Stack();

    while (!stack.isEmpty()) {
      TreeNode node = (TreeNode) stack.pop();
      list.add(node.val);
      if (node.right != null) {
        stack1.add(node.right);
      }
      if (node.left != null) {
        stack1.add(node.left);
      }
    }

    while (!stack1.isEmpty()) {
      nodeQueue.add(stack1.pop());
    }

    return nodeQueue;
  }

  /**
   * 给定一个二叉树，返回该二叉树层序遍历的结果，（从左到右，一层一层地遍历）
   *
   * @param root TreeNode类
   * @return int整型ArrayList<ArrayList<>>
   */
  public ArrayList<ArrayList<Integer>> levelOrder(TreeNode root) {
    // write code here

    ArrayList<ArrayList<Integer>> tree = new ArrayList();

    if (root == null) return tree;

    Queue queue = new ArrayDeque();

    queue.add(root);

    while (!queue.isEmpty()) {
      ArrayList<Integer> list = new ArrayList<Integer>();
      queue = treeLevel(queue, list);
      if (!list.isEmpty()) {
        tree.add(list);
      }
    }

    return tree;
  }

  public Queue treeLevel(Queue queue, ArrayList<Integer> list) {

    Queue nodeQueue = new ArrayDeque();

    while (!queue.isEmpty()) {
      TreeNode node = (TreeNode) queue.poll();
      list.add(node.val);
      if (node.left != null) {
        nodeQueue.add(node.left);
      }
      if (node.right != null) {
        nodeQueue.add(node.right);
      }

    }

    return nodeQueue;
  }

  /**
   * 求给定二叉树的最大深度，
   * 最大深度是指树的根结点到最远叶子结点的最长路径上结点的数量。
   *
   * @param root TreeNode类
   * @return int整型
   */
  public int maxDepth(TreeNode root) {
    // write code here
    return treeDepth(root);
  }

  public int treeDepth(TreeNode root) {
    if (root == null) return 0;

    int left = treeDepth(root.left);
    int right = treeDepth(root.right);

    return left >= right ? left + 1 : right + 1;

  }

  /**
   * 二叉树 求深度
   */
  public int treeDepthQueue(TreeNode root) {
    if (root == null) return 0;

    Queue<TreeNode> queue = new ArrayDeque();
    queue.add(root);
    int deep = 0, count = 0, nextCount = 1;
    while (!queue.isEmpty()) {
      TreeNode node = queue.poll();

      count++;

      if (node.left != null) {
        queue.add(node.left);
      }
      if (node.right != null) {
        queue.add(node.right);
      }

      if (count == nextCount) {
        nextCount = queue.size();
        count = 0;
        deep++;
      }
    }

    return deep;
  }


    /**
     * 输入一棵二叉树，判断该二叉树是否是平衡二叉树。
     * 在这里，我们只需要考虑其平衡性，不需要考虑其是不是排序二叉树
     * 平衡二叉树（Balanced Binary Tree），具有以下性质：
     *  它是一棵空树或它的左右两个子树的高度差的绝对值不超过1，并且左右两个子树都是一棵平衡二叉树。
     *
     *  递归实现
     * @param root
     * @return
     */
    public boolean isBalanced_Solution(TreeNode root) {

        if(root == null) return true;

        boolean[] isBalanced = {true};
        getHight(root, 1, isBalanced);

        return isBalanced[0];
    }

    /**
     * 获取树高度
     * @param node
     * @param level
     * @return
     */
    public int getHight(TreeNode node, int level,  boolean[] isBalanced){
        if(node == null) return level;

        int lLevel = getHight(node.left, level + 1, isBalanced);
        int rLevel = getHight(node.right,level + 1, isBalanced);

        if(Math.abs(lLevel - rLevel) > 1){
            isBalanced[0] = false;
        }

        return Math.max(lLevel, rLevel);
    }

    /**
     * 给定一棵二叉树，已经其中没有重复值的节点，请判断该二叉树是否为搜索二叉树和完全二叉树。
     *
     * @param root TreeNode类 the root
     * @return bool布尔型一维数组
     */
    public boolean[] judgeIt (TreeNode root) {
        // write code here
        boolean[] judge = {true, true};
        if(root == null) return  judge;
        isSearchTree(root, judge);
        isCompleteTree(root,judge);

        return  judge;
    }

    /**
     * 使用层次遍历
     * @param root
     * @param judge
     */
    public void isCompleteTree(TreeNode root, boolean[] judge){
        if(root == null) return;

        Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
        queue.offer(root);
        boolean isLeaf = false;

        while (!queue.isEmpty()){
            TreeNode node = queue.poll();
            if(isLeaf && (node.left != null || node.right != null)){
              judge[1] = false;
              return;
            }
            if(node.left == null && node.right != null){
                judge[1] = false;
                return;
            }

            if(node.left != null)
                queue.offer(node.left);

            if(node.right != null)
                queue.offer(node.right);
            else
              isLeaf=true;

        }
    }

    /**
     * 是否为搜索二叉树
     * @param root
     * @param judge
     */
    TreeNode pre = null;
    public void isSearchTree(TreeNode root, boolean[] judge){

        if(root == null) return;

        isSearchTree(root.left, judge);

        if(pre != null && pre.val >= root.val){
          judge[0] = false;
          return;
        }else{
          pre = root;
        }

        isSearchTree(root.right, judge);
    }

    /**
     *
     * 给定一个仅包含数字0−9 的二叉树，每一条从根节点到叶子节点的路径都可以用一个数字表示。
     * 例如根节点到叶子节点的一条路径是1→2→3,那么这条路径就用123 来代替。
     * 找出根节点到叶子节点的所有路径表示的数字之和
     *
     * @param root TreeNode类
     * @return int整型
     */
    public int sumNumbers (TreeNode root) {
      // write code here
      return afterTraversal(root);

    }
    Stack<Integer> stack = new Stack();
    public int afterTraversal(TreeNode root){

      if(root == null) return 0;

      if(root.left == null && root.right == null){
        if(stack.isEmpty()) return root.val;
        else{
          String sum = "";
          for(int i = 0; i< stack.size(); i++){
            sum =sum + stack.get(i) + "";
          }
          return Integer.valueOf(sum + root.val);
        }
      }
      if(root.left != null || root.right != null){
        stack.push(root.val);
      }
      int lsun = afterTraversal(root.left);
      int rsum = afterTraversal(root.right);
      if(!stack.isEmpty()){
        stack.pop();
      }

      return  lsun + rsum;
    }

  /**
   * 给定一个二叉树和一个值\ sum sum，请找出所有的根节点到叶子节点的节点值之和等于\ sum sum 的路径，
   * @param root TreeNode类
   * @param sum int整型
   * @return int整型ArrayList<ArrayList<>>
   */
  static ArrayList<ArrayList<Integer>> lists = new ArrayList<ArrayList<Integer>>();
  public static ArrayList<ArrayList<Integer>> pathSum (TreeNode root, int sum) {
    // write code here

    if(root == null) return lists;
    pathSumTree(root, sum);
    return lists;
  }

  static Stack<Integer> stackTree = new Stack();
  public static void pathSumTree(TreeNode root, int sum){

    if(root == null) return;

    if(root.left == null && root.right == null){
      ArrayList<Integer> arrayList = new ArrayList<Integer>();

      if(stackTree.isEmpty()){
        if(root.val == sum){
          arrayList.add(root.val);
          lists.add(arrayList);
        }

      }else{
        int sum1 = 0;

        for(int i = 0; i< stackTree.size(); i++){
          sum1 += stackTree.get(i);
          arrayList.add(stackTree.get(i));
        }
        arrayList.add(root.val);

        if((sum1 + root.val) == sum){
          lists.add(arrayList);

        }
      }
      return;
    }else{
      stackTree.push(root.val);
    }
    pathSum(root.left, sum);
    pathSum(root.right, sum);

    if(!stackTree.isEmpty()){
      stackTree.pop();
    }
  }

  /**
   * 给定一棵二叉树，判断其是否是自身的镜像（即：是否对称）
   *
   * @param root TreeNode类
   * @return bool布尔型
   */
  public boolean isSymmetric (TreeNode root) {
    if(root == null) return true;

    return recursionSymmetryTree(root.left, root.right);
  }

  /**
   * 二叉树是否对称，递归写法
   * @param left
   * @param right
   * @return
   */
  public boolean recursionSymmetryTree(TreeNode left, TreeNode right){

    if(left == null && right == null) return true;

    if(left == null || right == null) return false;

    return left.val == right.val && recursionSymmetryTree(left.left, right.right) && recursionSymmetryTree(left.right, right.left);
  }

  public boolean iterationSymmetryTree(TreeNode root){
    if(root == null) return true;

    Queue<TreeNode> queue = new LinkedList();
    queue.offer(root.left);
    queue.offer(root.right);

    while (!queue.isEmpty()){
      TreeNode left = queue.poll();
      TreeNode right = queue.poll();

      if(left == null && right == null) continue;

      if(left == null || right == null) return false;

      if(left.val != right.val) return  false;

      queue.offer(left.left);
      queue.offer(right.right);
      queue.offer(left.right);
      queue.offer(right.left);
    }
    return true;

  }

  /**
   * 给定一个二叉树和一个值\ sum sum，判断是否有从根节点到叶子节点的节点值之和等于\ sum sum 的路径，
   * 例如：
   * 给出如下的二叉树sum=22， 如路径和加在一起等于22，返回true，如不存在返回false
   *
   * @param root TreeNode类
   * @param sum int整型
   * @return bool布尔型
   */

  public boolean hasPathSum (TreeNode root, int sum) {
    // write code here
    if(root == null) return false;

    return recursionHasPathSum(root, sum);
  }
  //使用栈存储路径
  Stack<TreeNode> stackPathSum = new Stack();
  public boolean recursionHasPathSum(TreeNode root, int sum){

    if(root == null) return false;

    //left、righ为空说明是叶子节点
    if(root.left == null && root.right == null){
      //根据stack路径计算和
      if(!stackPathSum.isEmpty()){
        int stackSum = root.val;
        for(int i = 0; i < stackPathSum.size(); i++){
          stackSum += stackPathSum.get(i).val;
        }
        return stackSum == sum;
      }else{
        return root.val == sum;
      }
    }else{
      //把root存放在路径上
      stackPathSum.push(root);
      boolean leftBoolean = recursionHasPathSum(root.left, sum);
      boolean rightBoolean = recursionHasPathSum(root.right, sum);
      stackPathSum.pop();
      return  leftBoolean || rightBoolean;
    }
  }

  public boolean iterationHasPathSum(TreeNode root, int sum){
    return  false;
  }


  /**
   * 树的直径
   * 输入： 6,[[0,1],[1,5],[1,2],[2,3],[2,4]],[3,4,2,1,5]
   * 输出： 11
   * @param n int整型 树的节点个数
   * @param Tree_edge Interval类一维数组 树的边
   * @param Edge_value int整型一维数组 边的权值
   * @return int整型
   */
  public static int solveMaxEdgeValue (int n, Interval[] tree_edge, int[] edge_value) {
    // write code here
    if(tree_edge == null || edge_value == null || n <=1) return 0;

    Map<Integer, List<Edge>> greph = createGraph(tree_edge, edge_value);
    //其中一个节点
    int[] nodes_1 = {0,0};
    dfs(tree_edge[0].start, -1, greph,0, nodes_1);
    int[] nodes_2 = {0,0};
    dfs(nodes_1[1], -1, greph,0, nodes_2);
    return  nodes_2[0];
  }

  public static void dfs(int node, int preNode, Map<Integer, List<Edge>> greph, int path, int[]nodes){

    if(node < 0 || greph.get(node) == null) return;

    for(Edge edge : greph.get(node)){
      if(edge.end != preNode){
        path += edge.w;

        if(path > nodes[0]){
          nodes[0] = path;
          nodes[1] = edge.end;
        }
        dfs(edge.end, node, greph, path, nodes);
        path -= edge.w;

      }
    }
  }

  public static int maxEdgeValue(int currentNode, int preNode, Map<Integer, List<Edge>> graph){

    List<Edge> edges = graph.get(currentNode);

    int maxValue = 0;
    for(Edge edge : edges){
      int end = edge.end;

      if(preNode != -1)
        if(preNode == end) continue;

      preNode = currentNode;
      maxValue = Math.max(edge.w + maxEdgeValue(end, preNode, graph), maxValue);
    }
    return maxValue;
  }

  public static Map<Integer, List<Edge>> createGraph(Interval[] treeEdge, int[] edgeValue){

    Map<Integer, List<Edge>> map = new HashMap<Integer, List<Edge>>();

    for(int i = 0; i < treeEdge.length; i++){
      int start = treeEdge[i].start;
      int end = treeEdge[i].end;
      int w = edgeValue[i];

      if(!map.containsKey(start)){
        map.put(start, new ArrayList<Edge>());
      }
      map.get(start).add(new Edge(end, w));

      if(!map.containsKey(end)){
        map.put(end, new ArrayList<Edge>());
      }
      map.get(end).add(new Edge(start, w));
    }

    return map;
  }


  private int diameter = 0;
  public int solve11 (int n, Interval[] Tree_edge, int[] Edge_value) {
    // 使用数组保存所有的节点
    List<Edge>[] graph = new List[n];
    for (int i = 0; i < n; i++) {
      graph[i] = new ArrayList<Edge>();
    }
    for (int i = 0; i < Tree_edge.length; i++) {
      Interval interval = Tree_edge[i];
      int value = Edge_value[i];
      // 由于是无向图，所有每条边都是双向的
      graph[interval.start].add(new Edge(interval.end, value));
      graph[interval.end].add(new Edge(interval.start, value));
    }
    // 随机从一个节点开始dfs，这里选择的是0
    dfs(graph, 0, -1);
    return diameter;
  }
  // dfs返回值为从node节点开始的最长深度
  private int dfs(List<Edge>[] graph, int node, int parent) {
    int maxDepth = 0; //从节点开始的最长深度
    int secondMaxDepth = 0; //从节点开始的第二长深度
    for (Edge edge : graph[node]) {
      int neighbor = edge.end;
      if (neighbor == parent) continue; // 防止返回访问父节点
      int depth = edge.w+ dfs(graph, neighbor, node);
      if (depth > maxDepth) {
        secondMaxDepth = maxDepth;
        maxDepth = depth;
      }else if (depth > secondMaxDepth) {
        secondMaxDepth = depth;
      }
    }
    // maxDepth + secondMaxDepth为以此节点为中心的直径
    diameter = Math.max(diameter, maxDepth + secondMaxDepth);
    return maxDepth;
  }


  /**
   *
   * @param root TreeNode类
   * @return int整型
   */
  int maxValue = Integer.MIN_VALUE;
  public int maxPathSum (TreeNode root) {
    // write code here
    getMaxValue(root);
    return maxValue;
  }

  public int getMaxValue(TreeNode root){

    if(root == null) return 0;
    int sum = root.val;
    int maxLeft = getMaxValue(root.left);
    int maxRight = getMaxValue(root.right);
    if(maxLeft > 0) sum += maxLeft;
    if(maxRight > 0) sum += maxRight;
    maxValue = Math.max(maxValue, sum);

    return Math.max(Math.max(maxLeft+root.val, maxRight+root.val), root.val);
  }

  /**
   * 从上到下按层打印二叉树，同一层结点从左至右输出。每一层输出一行。
   *
   * 层级遍历
   *
   * @param pRoot
   * @return
   */
  public ArrayList<ArrayList<Integer>> Print(TreeNode pRoot) {
    ArrayList<ArrayList<Integer>> list = new ArrayList<ArrayList<Integer>>();

    if(pRoot == null) return list;

    ArrayList<Integer> rootList = new ArrayList<Integer>();

    int count = 0, newCount = 1;
    Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
    queue.offer(pRoot);

    while (!queue.isEmpty()){
      TreeNode node = queue.poll();
      ++count;
      rootList.add(node.val);
      if(node.left != null) queue.offer(node.left);
      if(node.right != null) queue.offer(node.right);
      if(count == newCount){
        count = 0;
        newCount = queue.size();
        list.add(rootList);
        rootList = new ArrayList<Integer>();
      }
    }

    return list;
  }



  /**
   *
   * 已知两颗二叉树，将它们合并成一颗二叉树。
   * 合并规则是：都存在的结点，就将结点值加起来，否则空的位置就由另一个树的结点来代替。
   *
   *
   * @param t1 TreeNode类
   * @param t2 TreeNode类
   * @return TreeNode类
   */
  public TreeNode mergeTrees (TreeNode t1, TreeNode t2) {
    // write code here
    if(t1  == null) return t2;
    if(t2 == null) return t1;
    mergeTree(t1, t2);

    return t1;
  }

  public TreeNode mergeTree (TreeNode t1, TreeNode t2) {
    // write code here
    if(t2 == null && t1 == null) return null;
    if(t2 == null) return t1;

    if(t1 == null) return t2;

    t1.val = t1.val + t2.val;

    t1.left = mergeTree(t1.left,t2.left);
    t1.right = mergeTree(t1.right,t2.right);

    return t1;
  }

  /**
   * 操作给定的二叉树，将其变换为源二叉树的镜像。
   *
   * @param pRoot
   * @return
   */
  public TreeNode Mirror (TreeNode pRoot) {
    // write code here
    if (pRoot == null) return null;

    mirrorPer(pRoot);

    return pRoot;

  }

  public void mirrorPer (TreeNode pRoot) {

    if(pRoot == null) return;

    TreeNode temp = pRoot.left;
    pRoot.left = pRoot.right;
    pRoot.right = temp;

    mirrorPer(pRoot.left);
    mirrorPer(pRoot.right);
  }



}

class Edge {
  int end;
  int w;
  Edge(int end, int w){
    this.w = w;
    this.end = end;
  }
}
class Point {
  int x;
  int y;

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }
}

class Interval {
    int start;
    int end;
    Interval(int start, int end){
      this.start = start;
      this.end = end;
    }
}

class TreeNode {
  int val;
  TreeNode left;
  TreeNode right;

  TreeNode(int x) {
    val = x;
  }
}