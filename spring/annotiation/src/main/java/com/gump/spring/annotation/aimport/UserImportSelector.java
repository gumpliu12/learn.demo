package com.gump.spring.annotation.aimport;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;

/**
 * @program: Learn.demo
 * @description:
 * @author: gumpliu
 * @create: 2019-06-11 12:04
 **/
public class UserImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        Map<String, Object> map =  annotationMetadata.getAnnotationAttributes(EnableUserService.class.getName(), true);
        for(Map.Entry<String, Object> entry : map.entrySet()){
            System.out.println("key is : " + entry.getKey() + " value is : " + entry.getValue());
        }

        return new String[]{UserServiceImpli.class.getName()};
    }

    public static void main(String[] args) {
//        s = 7, nums = [2,3,1,2,4,3] 输出：2 解释：子数组 [4,3] 是该条件下的长度最小的子数组

        /**
         *   2 3 1 1 5 1
         * 1 1 1 1 1 1 1
         * 2 1 1 1 1 1 1
         * 3 0 1 1 1 1 1
         * 4 0 1
         * 5
         * 6
         * 7
         */

    }
}
